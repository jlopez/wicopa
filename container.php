<?php


session_start ();

require_once "./dao/DBquery.php";

$db = new DBquery();

$type = "all";
$author = "";
$labels = array();
$tags = array();
$descriptions = array();

$rawtags = $db->getAllPublicTags();
$alltags = array();
foreach( $rawtags as $tgs) {
    $etags = explode(" ", $tgs);
    foreach( $etags as $tgs2) {
        array_push($alltags, $tgs2);
    }
}
$alltags = array_count_values($alltags);
arsort($alltags);
$popularTags = array_slice($alltags, 0, 5);


if(isset($_GET['type'])) {
    $type = $_GET['type'];
}

if(isset($_GET['mycontainer'])) {
    $author = $_SESSION['username'];
}

if(isset($_GET['labels'])) {
    foreach( $_GET['labels'] as $n) {
        array_push($labels, $n);
    }
}

if(isset($_GET['tags'])) {
    $tags = explode(" ", $_GET['tags']);
}

if(isset($_GET['descriptions'])) {
    $descriptions = explode(" ", $_GET['descriptions']);
}

$containers = $db->getContainers($type, $author, $labels, $tags, $descriptions);

require_once "./inc/php/buildHeader.php";
?>

<div class="container-fluid">

    <br/><br/>

    <div class="row justify-content-md-center text-center">
        <div class="col-sm-2">
            <div class="card border-primary sm-2">
            <div class="card-header font-weight-bold text-white bg-primary"><h4>Docker public</h4></div>
            <div class="card-body text-primary">
                <h5 class="card-title"><?php echo $db->getNumberDockerContainer() ?> Files</h5>
            </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="card border-success sm-2">
            <div class="card-header font-weight-bold text-white bg-success"><h4>Singularity public</h4></div>
            <div class="card-body text-success">
                <h5 class="card-title"><?php echo $db->getNumberSingularityContainer() ?> Files</h5>      
            </div>
            </div>
        </div>
    </div>

    <br/><br/>

    <div class="row">

    <div id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-info" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Filter
                    </button>
                    <a href="./container.php" class="btn btn-success">
                    Reset
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body col-12">
                    <form method="get">
                        <div class="form-inline form-group">
                            <label for="containerType">Container type :</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="type" id="containerType0" value="All" checked>
                                <label class="form-check-label" for="containerType0">
                                    All
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="type" id="containerType1" value="Singularity">
                                <label class="form-check-label" for="containerType1">
                                    Singularity
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="type" id="containerType2" value="Docker">
                                <label class="form-check-label" for="containerType2">
                                    Docker
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="labels">Labels :</label>
                            <select multiple class="form-control col-12" id="labels[]" name="labels[]">
                           
                            <?php
                                foreach ($db->getLabels() as $label) {
                                   echo '<option value="'.$label->ID.'">'.$label->name.'</option>';
                                }
                            ?>
                            </select>
                            <small id="emailHelp" class="form-text text-muted">Multiple choises is possible.</small>
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags :</label>
                            <input type="text" class="form-control" id="tags" name="tags">
                        </div>
                        <div class="form-group">
                            <label for="tags">Description :</label>
                            <input type="text" class="form-control" id="descriptions" name="descriptions">
                        </div>
                        <?php
                            if(isset($_SESSION['username'])) {
                                echo '
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="mycontainer" name="mycontainer">
                                    <label class="form-check-label" for="mycontainer">Only my containers</label>
                                    <br/><br/>
                                </div>
                                ';
                            }
                        ?>
                        <div class="form-inline form-group">
                            <label for="containerType">Order by :</label>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="order" name="order" checked>
                                <label class="form-check-label" for="order">Date</label>
                                <br/><br/>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Find</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    </div>

    <br/>

    <div class="row">
        <div class="col-12">
        <h3 >Top public tags :</h3>

        <?php
            foreach ($popularTags as $key => $value){
                echo '<span class="badge"><h6>'.$key.'</h6></span>'; 
            }
        ?>
            
        </div>
    </div>

    <br/><br/>

    <div class="row">
        <div class="col-12">
            <table id="Table_Container" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Author</th>
                        <th>Labels</th>
                        <th>Tags</th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="TableSection">
                <?php

                    foreach ($containers as $container) {

                        $container->escape2($db);

                        $visibility = "private";
                        $colorV = "e67e22";


                        if($container->visibility) {
                            $visibility = "public";
                            $colorV = "f1c40f";
                        }

                        if(!$container->visibility) {
                            if($container->author != $_SESSION['username']) {
                                if($db->getGradeWithLogin($_SESSION['username'])->name != $db->getMaxGrades()->name) {
                                    continue;
                                }
                            }
                        }

                        $typeC = "docker";
                        $colorT = "007bff";

                        if($container->type == "Singularity") {
                            $typeC = "singularity";
                            $colorT = "28a745";
                        }

                        echo '<tr>';
                        echo '<td>' . $container->name . '</td>';
                        echo '<td>' . $container->author . '</td>';


                        $labels = $db->getLabelWithContainerId($container->ID);

                        echo '<td>';
                        echo '<span class="badge" style="background:#'.$colorT.'">'.$typeC.'</span>';
                        echo '<span class="badge" style="background:#'.$colorV.'">'.$visibility.'</span>';

                        echo '<br/>';

                        $index = 0;

                        foreach ($labels as $label) {
                            echo '<span class="badge" style="background:#'.$label->color.'">'.$label->name.'</span>';

                            if($index == 1) {
                                echo '<br/>';
                                $index = 0;
                            } else {
                                $index++;
                            }       
                        }
                        
                        echo '</td>';

                        $tags = explode( ' ', $container->tags );

                        $index = 0;

                        echo '<td>';

                        foreach ($tags as $tag) {
                            echo '<span class="badge" style="background:#'."30336b".'; color:#FFFFFF">'.$tag.'</span>';  
                        }

                        echo  '</td>';
                        echo '<td>' . $container->date . '</td>';
                        echo '<td>' . $container->description . '</td>';

                        echo '<td>';

                        echo '<div class="btn-group mr-1" ><button class="btn btn-success btn-sm" onclick="showFileContainer(\''.$container->ID.'\')">show</button></div>';

                        echo '<div class="btn-group mr-1" ><a class="btn btn-primary btn-sm" href="./action/action_container.php?action=download&containerid='.$container->ID.'">dowload</a></div>';

                        if($container->author == $_SESSION['username'] || $_SESSION['manage'] ) {
                            //echo '<div class="btn-group mr-1"><a class="btn btn-warning btn-sm" href="./action/action_container.php?action=update&containerid='.$container->ID.'" >update</a></div>';
                        }

                        if($container->author == $_SESSION['username'] || $_SESSION['manage'] ) {
                            echo '<div class="btn-group mr-1"><a class="btn btn-danger btn-sm" href="./action/action_container.php?action=delete&containerid='.$container->ID.'" >delete</a></div>';
                        }
                        
            
                        echo '</td>';
                        echo '</tr>';
                    }

                ?>

                </tbody>
            </table>
        </div>
    </div>

    <!-- Start Modal Publish -->
    <div class="modal" id="modalShowContainer">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header">
                    <h4 class="modal-title">Container file</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <textarea type="text" class="form-control" id="valueContainerFile" name="value" value=""></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Import</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
    <!-- End Modal Publish -->

    <br/><br/><br/><br/><br/><br/>

</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>


<script type="text/javascript">

function showFileContainer(id) {
    var value = FILE_CONTAINER[id];

    document.getElementById('valueContainerFile').value = value;

    var size = value.split(/\r\n|\r|\n/).length;

    document.getElementById("valueContainerFile").rows = size; 

    $('#modalShowContainer').modal();
}

var FILE_CONTAINER = [];

$(function()
{
    <?php
        foreach ($containers as $container) {
            echo 'FILE_CONTAINER["'.$container->ID.'"] = "'.$container->value.'";
            ';
        }
    ?>
});

</script>

</body>

</html>