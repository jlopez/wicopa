<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 04/02/19
 * Time: 14:09
 */

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ./login.php");
}

require_once "./dao/DBquery.php";
require_once "./model/Section.php";

$db = new DBquery();

$sections_parent = $db->getAllSectionParent();

$managers = $db->getManagers();

$grades = $db->getGrades();

$max_arrangement = 0;

foreach ($sections_parent as $section) {
    if($section->arrangement > $max_arrangement) {
        $max_arrangement = $section->arrangement;
    } 
}

$max_grade = 0;

foreach ($grades as $grade) {
    if($grade->level > $max_grade) {
        $max_grade = $grade->level;
    } 
}

require_once "./inc/php/buildHeader.php";

?>

<div class="container-fluid">

    <br/>

    <div class="row">



        <div class="col-12">

                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Manage section :
                    </div>
                    <div class="card-body">
                        <table id="Table_Section" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Active</th>
                                    <th>Table name</th>
                                    <th>Panel name</th>
                                    <th>Color</th>
                                    <th>Arrangement</th>
                                    <th>SubArrangement</th>
                                    <th>Parent</th>
                                    <th>Save</th>
                                </tr>
                            </thead>
                            <tbody id="TableSection">
                      

                                    <?php

                                        foreach ($sections_parent as $section) {

                                            echo '<form action="./action/action_section.php" method="post"><tr>';
                                            
                                            if($section->active) {
                                                echo '<td>' . '<input type="checkbox" name="active" value="'.$section->ID.'" checked>' . '</td>';
                                            } else {
                                                echo '<td>' . '<input type="checkbox" name="active" value="'.$section->ID.'">' . '</td>';
                                            }
                                            
                                            echo '<td><input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                                            <input type="text" style="display: none;" name="sectionid" value="'.$section->ID.'"><input class="form-control" type="text" name="name" value="' . $section->name . '" readonly ></td>';
                                            echo '<td><input class="form-control" type="text" name="visual" value="' . $section->visual . '"></td>';
                                            echo '<td>' . '<input class="jscolor form-control" name="color" value="'.$section->color.'">' . '</td>';
                                            echo '<td>' . '<input id="number" name="arrangement" class="form-control" type="number" value="'.$section->arrangement.'">' . '</td>';
                                            echo '<td>' . "" . '</td>';
                                            echo '<td>' . $section->parent . '</td>';
                                            echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm">save</button>' . '</td>';
                                            echo '</tr></form>';

                                            foreach ($db->getAllSectionWhithParent($section->name) as $subsection) {
                                                echo '<form action="./action/action_section.php" method="post"><tr>';
                                            
                                                if($subsection->active) {
                                                    echo '<td>' . '<input type="checkbox" name="active" value="'.$subsection->ID.'" checked>' . '</td>';
                                                } else {
                                                    echo '<td>' . '<input class="btn" type="checkbox" name="active" value="'.$subsection->ID.'">' . '</td>';
                                                }
                                                
                                                echo '<td><input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                                                <input type="text" style="display: none;" name="sectionid" value="'.$subsection->ID.'"><input class="form-control" type="text" name="name" value="' . $subsection->name . '" readonly ></td>';
                                                echo '<td><input class="form-control" type="text" name="visual" value="' . $subsection->visual . '"></td>';
                                                echo '<td>' . '<input class="jscolor form-control" name="color" value="'.$subsection->color.'">' . '</td>';
                                                echo '<td>' . "" . '</td>';
                                                echo '<td>' . '<input id="number" name="arrangement" class="form-control" type="number" value="'.$subsection->arrangement.'">' . '</td>';
                                                echo '<td>' . $subsection->parent . '</td>';
                                                echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm">save</button>' . '</td>';       
                                                echo '</tr></form>'; 
                                            }
                                        }

                                    ?>

                                                                        
                            </tbody>
                        </table>

    


                        <br/>
                        <br/>
                        <br/>
                        <br/>

                        <form action="./action/action_section.php" method="post">
                        <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                        <div class="row">
                            <div class="col-2">
                                <label for="nameSection">Table name :</label>
                                <input type="text" class="form-control" id="nameSection" name="name" >
                            </div>
                            <div class="col-2">
                                <label for="visualSection">Panel name :</label>
                                <input type="text" class="form-control" id="visualSection" name="visual" >
                            </div>
                            <div class="col-2">
                                <label for="colorSection">Color :</label>
                                <input class="jscolor form-control" name="color" value="#FFFFFF">
                            </div>
                            <div class="col-2">
                                <label for="arrangementSection">Arrangement :</label>
                                <input id="arrangementSection" name="arrangement" class="form-control" type="number" value="<?php echo $max_arrangement + 1; ?>">
                            </div>
                            <div class="col-12">
                                <br/>
                            <button type="submit" class="btn btn-success">Create</button>
                            </div>
                        </div>


                    </form>

                    <br/>

                        <p> If you want create a section you must first create table in database. </p>

                    </div>
                </div>

        </div>

        </div>

        <br/>

    <div class="row">

        <div class="col-12">

            <div class="card border border-dark">
                <div class="card-header bg-dark text-light" id="distribution">
                    Manage distribution :
                </div>
                <div class="card-body">
                    <table id="Table_Distrib" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Active</th>
                                        <th>Name</th>
                                        <th>Version</th>
                                        <th>Manager</th>
                                        <th>Vname</th>                            
                                        <th>Save</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody id="TableSection">
                                    <?php

                                        foreach ($db->getDistributions() as $dist) {
                                            echo '<form action="./action/action_distribution.php" method="post"><tr>';


                                            echo '<td>
                                            <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                                            <input type="text" style="display: none;" name="distribid" value="'.$dist->ID.'">';
                                            if($dist->active == 1) {
                                                echo '<input type="checkbox" name="active" value="'.$dist->ID.'" checked>';
                                            } else {
                                                echo '<input type="checkbox" name="active" value="'.$dist->ID.'">';
                                            }

                                            echo '</td>';
                                   
                                            echo '<td > <input class="form-control" type="text" name="name" value="' . $dist->name . '" ></td>';
                                            echo '<td > <input class="form-control" type="text" name="version" value="' . $dist->version . '" ></td>';

                                            echo '<td><select name="manager" class="form-control">';
                                            foreach ($managers as $manager) {

                                                if($manager->value == $dist->manager) {
                                                    echo '<option value="'.$manager->value.'" selected="selected">'.$manager->name.'</option>';
                                                } else {
                                                    echo '<option value="'.$manager->value.'">'.$manager->name.'</option>';
                                                }
                                            }
                                            echo '</select></td>';

                                            echo '<td > <input class="form-control" type="text" name="vname" value="' . $dist->vname . '" ></td>';
                                            echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm">save</button>' . '</td>';
                                            echo '<td>' . '<a class="btn btn-danger btn-sm" href="./action/action_distribution.php?action=delete&distribid='.$dist->ID.'" >x</a>' . '</td>';

                                            echo '</tr> </form>';
                                        }

                                    ?>

                                </tbody>
                        </table>

                    
                        <br/>
                        <br/>

                    <form action="./action/action_distribution.php" method="post">
                    <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                        <div class="row" >
                            <div class="col-2">
                                <label for="nameManager">Name :</label>
                                <input type="text" class="form-control" id="nameManager" name="name" >
                            </div>
                            <div class="col-2">
                                <label for="versionManager">Version :</label>
                                <input type="text" class="form-control" id="versionManager" name="version" >
                            </div>
                            <div class="col-2">
                                <label for="vnameManager">Version name :</label>
                                <input type="text" class="form-control" id="vnameManager" name="vname" >
                            </div>
                            <div class="col-2">
                                <label for="managerManager">Manager :</label>
                                <select class="form-control" id="managerManager" name="manager">
                                <?php
                                foreach ($managers as $manager) {
                                    echo '<option value="'.$manager->value.'">'.$manager->name.'</option>';
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col-12">
                                <br/>
                            <button type="submit" class="btn btn-success">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <br/>

    <div class="row">

        <div class="col-12">

            <div class="card border border-dark">
                <div class="card-header bg-dark text-light" id="manager">
                    Manage distribution packaging tools :
                </div>
                <div class="card-body">
                    <table id="Table_Distrib" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <th>Name</th>
                                        <th>Value</th>  
                                        <th>Save</th>
                                        <th>Delete</th>            
                                    </tr>
                                </thead>
                                <tbody id="TableSection">
                                    <?php

                                    foreach ($managers as $manager) {

                                        echo '<form action="./action/action_manager.php" method="post"><tr>';
                                        echo '<td>' . '
                                        <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                                        <input type="text" style="display: none;" name="managerid" value="'.$manager->ID.'"> 
                                        <input class="form-control" type="text" name="name" value="'.$manager->name.'">' . '</td>';
                                        echo '<td>' . '<input class="form-control" type="text" name="value" value="'.$manager->value.'">' . '</td>';
                                        echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm" >save</button>' . '</td>';
                                        echo '<td>' . '<a class="btn btn-danger btn-sm" href="./action/action_manager.php?action=delete&managerid='.$manager->ID.'" >x</a>' . '</td>';
                                        echo '</tr></form>';
                                    }

                                    ?>
                                </tbody>
                        </table>

                        <!-- <button type="button" class="btn btn-primary" >Save change</button> -->

                        <br/>
                        <br/>
                        <br/>
                        <br/>

                    <form action="./action/action_manager.php" method="post">
                        <div  class="row">
                        <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                            <div class="col-2">
                                <label for="nameManager">Name :</label>
                                <input type="text" class="form-control" id="nameManager" name="name" >
                            </div>
                            <div class="col-2">
                                <label for="valueManager">Value :</label>
                                <input type="text" class="form-control" id="valueManager" name="value" >
                            </div>
                            <div class="col-12">
                                <br/>
                            <button type="submit" class="btn btn-success">Create</button>
                            </div>
                        </div>


                    </form>



                </div>

            </div>
        </div>
    </div>


    <br/>

    <div class="row">

        <div class="col-12">

            <div class="card border border-dark">
                <div class="card-header bg-dark text-light" id="grade">
                    Manage Grade :
                </div>
                <div class="card-body">
                    <table id="Table_Distrib" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <th>Name</th>
                                        <th>Level</th>  
                                        <th>Save</th>
                                        <th>Delete</th>            
                                    </tr>
                                </thead>
                                <tbody id="TableSection">
                                    <?php

                                    foreach ($grades as $grade) {

                                        echo '<form action="./action/action_grade.php" method="post"><tr>';
                                        echo '<td>' . '
                                        <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                                        <input type="text" style="display: none;" name="gradeid" value="'.$grade->ID.'"> 
                                        <input class="form-control" type="text" name="name" value="'.$grade->name.'">' . '</td>';
                                        echo '<td>' . '<input class="form-control" type="text" name="level" value="'.$grade->level.'">' . '</td>';
                                        echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm" >save</button>' . '</td>';
                                        echo '<td>' . '<a class="btn btn-danger btn-sm" href="./action/action_grade.php?action=delete&gradeid='.$grade->ID.'" >x</a>' . '</td>';
                                        echo '</tr></form>';
                                    }

                                    ?>
                                </tbody>
                        </table>

                        <br/>
                        <br/>
                        <br/>
                        <br/>

                    <form action="./action/action_grade.php" method="post">
                        <div  class="row">
                        <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                            <div class="col-2">
                                <label for="nameGrade">Name :</label>
                                <input type="text" class="form-control" id="nameGrade" name="name" >
                            </div>
                            <div class="col-2">
                                <label for="levelManager">Level :</label>
                                <input type="number" class="form-control" id="levelManager" name="level" value="<?php echo $max_grade + 1; ?>">
                            </div>
                            <div class="col-12">
                                <br/>
                            <button type="submit" class="btn btn-success">Create</button>
                            </div>
                        </div>


                    </form>



                </div>

            </div>

        </div>
    </div>

    <br/>

<div class="row">

    <div class="col-12">

        <div class="card border border-dark">
            <div class="card-header bg-dark text-light" id="label">
                Manage Label :
            </div>
            <div class="card-body">
                <table id="Table_Distrib" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    
                                    <th>Name</th>
                                    <th>Color</th>  
                                    <th>Minimum grade</th>  
                                    <th>Save</th>
                                    <th>Delete</th>            
                                </tr>
                            </thead>
                            <tbody id="TableSection">
                                <?php

                                foreach ($db->getLabels() as $label) {

                                    echo '<form action="./action/action_label.php" method="post"><tr>';
                                    echo '<td>' . '
                                    <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                                    <input type="text" style="display: none;" name="labelid" value="'.$label->ID.'"> 
                                    <input class="form-control" type="text" name="name" value="'.$label->name.'">' . '</td>';
                                    echo '<td>' . '<input class="jscolor form-control" name="color" value="'.$label->color.'">' . '</td>';
                                    echo '<td><select name="gradeId" class="form-control">';
                                    foreach ($grades as $grade) {

                                        if($grade->ID == $label->gradeId) {
                                            echo '<option value="'.$grade->ID.'" selected="selected">'.$grade->name.'</option>';
                                        } else {
                                            echo '<option value="'.$grade->ID.'">'.$grade->name.'</option>';
                                        }
                                    }
                                    echo '</select></td>';
                                    echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm" >save</button>' . '</td>';
                                    echo '<td>' . '<a class="btn btn-danger btn-sm" href="./action/action_label.php?action=delete&labelid='.$label->ID.'" >x</a>' . '</td>';
                                    echo '</tr></form>';
                                }

                                ?>
                            </tbody>
                    </table>

                    <br/>
                    <br/>
                    <br/>
                    <br/>

                <form action="./action/action_label.php" method="post">
                    <div  class="row">
                    <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                        <div class="col-2">
                            <label for="nameLabel">Name :</label>
                            <input type="text" class="form-control" id="nameLabel" name="name" >
                        </div>
                        <div class="col-2">
                                <label for="colorLabel">Color :</label>
                                <input class="jscolor form-control" name="color" value="#FFFFFF">
                        </div>
                        <div class="col-2">
                            <label >Grade :</label>
                            <select name="gradeId" class="form-control">
                        <?php

                            foreach ($grades as $grade) {
                                echo '<option value="'.$grade->ID.'">'.$grade->name.'</option>';                              
                            }

                        ?>
                            </select>
                        </div>
                        <div class="col-12">
                            <br/>
                        <button type="submit" class="btn btn-success">Create</button>
                        </div>
                    </div>


                </form>



            </div>

        </div>

    </div>
</div>

<br/>

<div class="row">

    <div class="col-12">

        <div class="card border border-dark">
            <div class="card-header bg-dark text-light" id="user">
                Manage Graded Users :
            </div>
            <div class="card-body">
                <table id="Table_Distrib" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    
                                    <th>Name</th>
                                    <th>Grade</th>           
                                </tr>
                            </thead>
                            <tbody id="TableSection">
                                <?php

                                

                                foreach ($db->getUsers() as $user) {

                                    echo '<form action="./action/action_user.php" method="post"><tr>';
                                    echo '<td>' . '
                                    <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                                    <input type="text" style="display: none;" name="userid" value="'.$user->ID.'"> 
                                    <input class="form-control" type="text" name="login" value="'.$user->login.'">' . '</td>';
                                    echo '<td><select name="gradeId" class="form-control">';
                                    foreach ($grades as $grade) {

                                        if($grade->ID == $user->gradeId) {
                                            echo '<option value="'.$grade->ID.'" selected="selected">'.$grade->name.'</option>';
                                        } else {
                                            echo '<option value="'.$grade->ID.'">'.$grade->name.'</option>';
                                        }
                                    }
                                    echo '</select></td>';
                                    echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm" >save</button>' . '</td>';
                                    echo '<td>' . '<a class="btn btn-danger btn-sm" href="./action/action_user.php?action=delete&userid='.$user->ID.'" >x</a>' . '</td>';
                                    echo '</tr></form>';
                                }

                                ?>
                            </tbody>
                    </table>

                    <br/>
                    <br/>
                    <br/>
                    <br/>

                <form action="./action/action_user.php" method="post">
                    <div  class="row">
                    <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                        <div class="col-2">
                            <label for="loginUser">Login :</label>
                            <input type="text" class="form-control" id="loginUser" name="login" >
                        </div>
                        <div class="col-2">
                            <label >Grade :</label>
                            <select name="gradeId" class="form-control">
                        <?php

                            foreach ($grades as $grade) {
                                echo '<option value="'.$grade->ID.'">'.$grade->name.'</option>';                              
                            }

                        ?>
                            </select>
                        </div>
                        <div class="col-12">
                            <br/>
                        <button type="submit" class="btn btn-success">Create</button>
                        </div>
                    </div>


                </form>



            </div>

        </div>

    </div>
</div>


    <br/><br/><br/><br/><br/><br/>

</div>

<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>

</html>
