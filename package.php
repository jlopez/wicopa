<?php

require_once(__DIR__.'/model/Package.php');

/**
 * Package tool function
 * 
 * @file package.php
 */

/**
 * Replace special character with empty string
 * 
 * @param string $value The value to clean
 * 
 * @return string
 * 
 */
function cleanValue($value) {
    $value = str_replace("\n", "", $value);
    $value = str_replace("\r", "", $value);
    $value = str_replace("\t", "", $value);
    $value = str_replace("  ", "", $value);
    return $value;
}

/**
 * Load HTML page from url and return a DOMDocument object
 * 
 * @param string url The url of the page
 * 
 * @return DOMDocument
 */
function getDocument($url) {
    $opts = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"Accept-language: en\r\n"
        )
    );
    $context = stream_context_create($opts);
    $content = file_get_contents($url, false, $context);

    $doc = new \DOMDocument('1.0', 'UTF-8');
    $internalErrors = libxml_use_internal_errors(true);
    $doc->loadHtml($content);
    libxml_use_internal_errors($internalErrors);
    return $doc;
}

/**
 * Find all packages from distribution
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findDistributionPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {
    if($os_name == "ubuntu" || $os_name == "debian") {
        return findUbuntuDebianPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname);
    }
    else if($os_name == "fedora" || $os_name == "centos") {
        return findFedoraCentosPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname);
    }
    else if($os_name == "alpine") {
        return findAlpinePackages($os_name, $version_num, $version_name, $keyword, $limit, $onlyname);
    }
}


/**
 * Find all packages from fedora or centos
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findFedoraCentosPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $keyword_first = strtolower($keyword[0]);

    $url_packages = "";

    if($os_name == "fedora") {
        $url_packages = "https://dl.fedoraproject.org/pub/fedora/linux/releases/".$version_num."/Everything/source/tree/Packages/".$keyword_first."/";
    }
    else if($os_name == "centos") {
        $url_packages = "http://mirror.centos.org/centos/".$version_num."/os/x86_64/Packages/";
    }

    $doc = getDocument($url_packages);

    $as = $doc->getElementsByTagName('a');

    $size = $as->length;

    $index = 0;

    for ($i = 5; $i <= $size; $i++) {
        
        $value = $as[$i]->nodeValue;

        if($value != null) {
            $subject = $value;
            $pattern = "#(.*)-(.*)-(.*?)\.(.*)(\.rpm)#";
            preg_match($pattern, $subject, $matches);
            $name = $matches[1];
            $version = $matches[2];

            $pos = strpos($name, $keyword);

            if($pos !== false) {
                $link = $url_packages.$value;
                $other = $os_name.":".$version_num;
                $description = "";
                $package = new Package($name, $version, $description, $link, $other);
                $results[] = $package;

                $index++;

                if($index == $limit) {
                    break;
                }
            }
        }
    }

    return $results;
}

/**
 * Find all packages from alpine
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findAlpinePackages($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $keyword = "*".$keyword."*";

    $basic_url = "https://pkgs.alpinelinux.org/packages";

    $url_packages = $basic_url."?name=".$keyword."&branch=v".$version_num."&repo=main&arch=x86_64";

    $doc = getDocument($url_packages);

    $tr = $doc->getElementsByTagName('tr');

    $size = $tr->length;

    $index = 0;

    for ($i = 1; $i <= $size; $i++) {

        $check = $tr[$i]->nodeValue;

	    if($check == null) {
           continue;
        }

        $name = cleanValue($tr[$i]->getElementsByTagName('td')[0]->nodeValue);
        $version = $tr[$i]->getElementsByTagName('td')[1]->nodeValue;

        if($name != null && $name != "") {

            $description = "";
            $link = "https://pkgs.alpinelinux.org/package/v".$version_num."/main/x86_64/".$name;
            $other = $os_name.":".$version_num;
            $package = new Package($name, $version, $description, $link, $other);
            $results[] = $package;
            $index++;

            if($index == $limit) {
                break;
            }
        }
    }

    return $results;
}

/**
 * Find all packages from ubuntu or debian
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findUbuntuDebianPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $filter = "all";

    if($onlyname) {
        $filter = "names";
    }

    $basic_url = "";

    if($os_name == "ubuntu") {
        $basic_url = "https://packages.ubuntu.com";
    }
    else if($os_name == "debian") {
        $basic_url = "https://packages.debian.org";
    }

    $url_packages = $basic_url."/search?suite=".$version_name."&section=all&arch=any&keywords=".$keyword."&searchon=".$filter;

    $doc = getDocument($url_packages);

    $list = $doc->getElementById('psearchres');

    $names = $list->getElementsByTagName('h3');
    $uls = $list->getElementsByTagName('ul');


    $size = $names->length;

    if($names->length != $uls->length) {
        $size = min($names->length, $uls->length);
    }

    $size = min($size, $limit);

    for ($i = 0; $i <= $size; $i++) {
        $name = substr($names[$i]->nodeValue, 8);

        if($name) {

            $description = $uls[$i]->childNodes[1]->childNodes[1]->nodeValue;
            $description = cleanValue(substr($description, 1, -2));    
            $version = $uls[$i]->childNodes[1]->childNodes[5]->nodeValue;

            if($version == null) {
                $version = $uls[$i]->childNodes[1]->childNodes[4]->nodeValue;
            }

            if($version == null) {
                $version = $uls[$i]->childNodes[1]->childNodes[3]->nodeValue;
            }
            
            if($version == null) {
                $version = "";
            } else {
                $version = explode (":", $version)[0];
            }

            $link = $basic_url."/".$version_name."/".$name;

            $other = $os_name.":".$version_num;

            $package = new Package($name, $version, $description, $link, $other);

            $results[] = $package;
    
        }

    }

    return $results;
} 


/**
 * Find all packages from python
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findPythonPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $basic_url = "https://pypi.org/search/?q=";
    
    $url_packages = $basic_url.$keyword;

    $doc = getDocument($url_packages);

    $tr = $doc->getElementsByTagName('li');

    $size = $tr->length;

    $index = 0;

    for ($i = 0; $i <= $size-1; $i++) {

        $vv = $tr[$i]->getElementsByTagName('span');

        if($vv->length != 0) {

            $name = $vv[0];

            $description = $tr[$i]->getElementsByTagName('p');

            if($name != null) {

                $name = $name->nodeValue;
                $version = $vv[1]->nodeValue;
                $description = $description[0]->nodeValue;
                $link = "https://pypi.org/project/$name";
                $other = "python";

                $package = new Package($name, $version, $description, $link, $other);
                $results[] = $package;

                $index++;
            }

            if($index == $limit) {
                break;
            }
        }
    }

    return $results;
}

/**
 * Find all R packages from CRAN, Bioconductor, Rforge or Github
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findRPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $repos = "";

    if($os_name == "CRAN") {
        $repos = "cran";
    } 
    else if($os_name == "Bioconductor") {
        $repos = "bioc";
    }
    else if($os_name == "Rforge") {
        $repos = "rforge";
    }
    else if($os_name == "Github") {
        $repos = "github";
    }

    $results = [];

    if($repos == "cran") {

        $json = file_get_contents("http://search.r-pkg.org/package/_search?size=$limit&q=Package:$keyword*");
        $json = json_decode($json, true);

        if($json["hits"]) {

            $packages = $json["hits"]["hits"];

            foreach ($packages as $package){

                $source = $package["_source"];

                $name = $source["Package"];
                $version = $source["Version"];
                $description = $source["Description"];
                $link = "https://cloud.r-project.org/web/packages/$name/index.html";
                $other = "Depends: ".$source["Depends"]."<br/>Imports : ".$source["Imports"];

                $package = new Package($name, $version, $description, $link, $other);
                $results[] = $package;
            }
        }
    }
    else if($repos == "bioc") {

        $url_packages = "https://www.bioconductor.org/packages/release/bioc/";

        $doc = getDocument($url_packages);
        
        $trs = $doc->getElementsByTagName('tr');

        $size = $trs->length - 1;

        $index = 0;

        for ($i = 1; $i <= $size; $i++) {
            $name = $trs[$i]->getElementsByTagName('td')[0]->nodeValue;

            if(strpos($name, $keyword) !== false) {
                $description = $trs[$i]->getElementsByTagName('td')[2]->nodeValue;
                $version = "";
                $link = "https://www.bioconductor.org/packages/release/bioc/html/".$name.".html";
                $other =  $trs[$i]->getElementsByTagName('td')[1]->nodeValue;
                $package = new Package($name, $version, $description, $link, $other);
                $results[] = $package;
                $index++;

            }

            if($index == $limit) {
                break;
            }
        }

    }

    return $results;

}

/**
 * Find all packages from CPAN
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findCPANPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $basic_url = "https://metacpan.org/search?size=".$limit."&q=";
    $url_packages = $basic_url.$keyword;

    $doc = getDocument($url_packages);
    $bigs = $doc->getElementsByTagName('big');
    $spans = $doc->getElementsByTagName('span');
    $finder = new DomXPath($doc);
    $classname="relatize";
    $spans = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

    $size = $bigs->length - 1;

    for ($i = 0; $i <= $size; $i++) {
        $splt = explode (" - ", $bigs[$i]->nodeValue);

        $name = cleanValue($splt[0]);
        $name = str_replace(" ", "", $name);
        $description = cleanValue($splt[1]);
        $link = "https://metacpan.org/pod/".$name;
        $version = "";
        $other = $spans[$i]->childNodes[0]->nodeValue;

        $package = new Package($name, $version, $description, $link, $other);
        $results[] = $package;
    }

    return $results;
}

/**
 * Find all packages from NPM
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findNPMPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $basic_url = "https://www.npmjs.com/search?q=";
    $url_packages = $basic_url.$keyword;
    $doc = getDocument($url_packages);
    $section = $doc->getElementsByTagName('section');
    $size = $section->length;

    $index = 0;

    for ($i = 0; $i <= $size; $i++) {

        $vv = $section[$i]->childNodes[1];
        $name = $vv->childNodes[0]->childNodes[0]->nodeValue;
        $description = $vv->childNodes[2]->childNodes[0]->nodeValue;
        $version == "";

        if($vv->childNodes->length == 5) {
            $version = $vv->childNodes[4]->childNodes[1]->nodeValue;
        } else if($vv->childNodes->length == 7) {
            $version = $vv->childNodes[6]->childNodes[1]->nodeValue;
        }

        $other = explode (" ", $version);
        $version = explode (" ", $version)[1];
        
        $other = $other[3]." ".$other[4]." ".$other[5];

        $link = "https://www.npmjs.com/package/".$name;
    
        $package = new Package($name, $version, $description, $link, $other);
        $results[] = $package;
        
        $index++;
    
        if($index == $limit) {
            break;
        }

    }

    return $results;
}

/**
 * Find all packages from Ruby
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findRubyPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $basic_url = "https://rubygems.org/search?page=1&query=";
    
    $url_packages = $basic_url.$keyword;

    $doc = getDocument($url_packages);

    $tr = $doc->getElementsByTagName('a');

    $size = $tr->length;

    $index = 0;

    for ($i = 9; $i <= $size; $i++) {

        $vv = $tr[$i]->childNodes[1];

        $name = cleanValue($vv->childNodes[1]->childNodes[0]->nodeValue);

        if($name != "" && $name != null) {
            $version = $vv->childNodes[1]->childNodes[1]->nodeValue;

            $description = $vv->childNodes[3]->nodeValue;
    
            $other = $tr[$i]->childNodes[3]->childNodes[0]->nodeValue;
    
            $link = "https://rubygems.org/gems/".$name;
    
            $package = new Package($name, $version, $description, $link, $other);
            $results[] = $package;
    
            $index++;
    
            if($index == $limit) {
                break;
            }
        }
    }

    $doc = getDocument($url_packages);

    return $results;

    
}

/**
 * Find all packages from Conda
 * 
 * @param string $os_name The name of the distribution
 * @param string $version_num The version of the distribution
 * @param string $version_name The version name of the distribution
 * @param string $keyword The keyword to search for in all packages of the distribution
 * @param int $limit The limit of package found
 * @param bool $onlyname If your search only on the name of the package. If the api of the distribution have this possibility.
 * 
 * @return array
 */
function findCondaPackage($os_name, $version_num, $version_name, $keyword, $limit, $onlyname) {

    $results = array();

    $basic_url = "https://anaconda.org/search?q=access%3Apublic+platform%3Alinux-64+type%3Aconda+";

    $url_packages = $basic_url.$keyword;

    $doc = getDocument($url_packages);

    $tr = $doc->getElementsByTagName('tr');

    $size = $tr->length;

    $index = 0;

    for ($i = 1; $i <= $size; $i++) {

        $check = $tr[$i]->nodeValue;

        if($check == null) {          
           continue;
         }

        $vv = $tr[$i]->getElementsByTagName('a')[1];

        $author = cleanValue($tr[$i]->getElementsByTagName('a')[0]->nodeValue);

        $name = $author . "/" . $vv->childNodes[1]->nodeValue;
        $version = $vv->childNodes[3]->nodeValue;

        $description = cleanValue($vv->parentNode->parentNode->childNodes[2]->nodeValue);
        $other = $tr[$i]->childNodes[3]->nodeValue;

        $link = "https://anaconda.org/conda-forge/".$name;

        $package = new Package($name, $version, $description, $link, $other);
        array_push($results, $package);
        $index++;

        if($index == $limit) {
            return $results;
        }
    }
    
    return $results;
}
