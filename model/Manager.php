<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 27/02/19
 * Time: 13:17
 */

require_once(__DIR__.'/../dao/DBquery.php');
require_once(__DIR__.'/../model/Model.php');

/**
 * Class Manager
 */
class Manager extends Model
{

    var $ID = -1;
    var $name = "";
    var $value = "";

    /**
     * Section constructor.
     * @param int $ID
     * @param string $name
     * @param int $value
     */
    public function __construct($ID, $name, $value)
    {
        $this->ID = $ID;
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO Manager (name, value)
        VALUES ('$this->name', '$this->value');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE Manager
        SET name='$this->name', value='$this->value'
        WHERE ID = '$this->ID';";

    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM Manager WHERE ID = '$this->ID';";
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
        $this->name  = utf8_decode($db->escape($this->name));
        $this->value = utf8_decode($db->escape($this->value));
    }

}