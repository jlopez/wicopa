<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 10/04/19
 * Time: 16:43
 */

require_once(__DIR__.'/../dao/DBquery.php');
require_once(__DIR__.'/../model/Model.php');

class Label extends Model
{
    var $ID    = 0;
    var $name  = "";
    var $color = "#FFFFFF";
    var $gradeId = 0;

    /**
     * Label constructor.
     * @param int $ID
     * @param string $name
     * @param string $color
     * @param int $gradeId
     */
    public function __construct($ID, $name, $color, $gradeId)
    {
        $this->ID      = $ID;
        $this->name    = $name;
        $this->color   = $color;
        $this->gradeId = $gradeId;
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
        $this->name  = utf8_decode($db->escape($this->name));
        $this->color = utf8_decode($db->escape($this->color));
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO Label (name, color, gradeId)
        VALUES ('$this->name', '$this->color', '$this->gradeId');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE Label
        SET name='$this->name', color='$this->color', gradeId='$this->gradeId'
        WHERE ID = '$this->ID';";

    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM Label WHERE ID = '$this->ID';";
    }

}
