<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 26/02/19
 * Time: 14:28
 */

require_once(__DIR__.'/../model/Model.php');

/**
 * Class Section
 */
class Section extends Model
{

    var $ID          = -1;
    var $name        = "";
    var $active      = 0;
    var $color       = "";
    var $arrangement = "";
    var $parent      = "";
    var $state       = 0;

    /**
     * Section constructor.
     * @param int $ID
     * @param string $name
     * @param string $visual
     * @param int $active
     * @param string $color
     * @param string $arrangement
     * @param string $parent
     */
    public function __construct($ID, $name, $visual, $active, $color, $arrangement, $parent)
    {
        $this->ID          = $ID;
        $this->name        = $name;
        $this->visual      = $visual;
        $this->active      = $active;
        $this->color       = $color;
        $this->arrangement = $arrangement;
        $this->parent      = $parent;
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO Section (name, visual, active, color, arrangement, state)
        VALUES ('$this->name', '$this->visual', '$this->active', '$this->color', '$this->arrangement' , '$this->state');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE Section
        SET name='$this->name', visual='$this->visual', active='$this->active', color='$this->color', arrangement='$this->arrangement', state='$this->state'
        WHERE ID = '$this->ID';";

    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM Section WHERE ID = '$this->ID';";
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
        $this->name  = utf8_decode($db->escape($this->name));
    }

    /**
     * Active update for this section
     */
    public function activateUpdate() {
        $this->state = 1;
    }


    /**
     * Deactive update for this section
     */
    public function deactivateUpdate() {
        $this->state = 0;
    }


}