<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 10/04/19
 * Time: 17:36
 */

require_once(__DIR__.'/../dao/DBquery.php');
require_once(__DIR__.'/../model/Model.php');

/**
 * Class Grade
 */
class Grade extends Model
{
    var $ID    = 0;
    var $name  = "";
    var $level = 1;

    /**
     * Grade constructor.
     * @param int $ID
     * @param string $name
     * @param int $level
     */
    public function __construct($ID, $name, $level)
    {
        $this->ID    = $ID;
        $this->name  = $name;
        $this->level = $level;
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
        $this->name  = utf8_decode($db->escape($this->name));
        $this->level = utf8_decode($db->escape($this->level));
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */   
    public function getInsert() {
        return "INSERT INTO Grade (name, level)
        VALUES ('$this->name', '$this->level');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE Grade
        SET name='$this->name', level='$this->level'
        WHERE ID = '$this->ID';";

    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM Grade WHERE ID = '$this->ID';";
    }
}