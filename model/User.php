<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 29/04/19
 * Time: 13:58
 */

require_once(__DIR__.'/../dao/DBquery.php');
require_once(__DIR__.'/../model/Model.php');

/**
 * Class User
 */
class User extends Model
{
    var $ID      = 0;
    var $login   = "";
    var $gradeId = 0;

    /**
     * User constructor.
     * @param int $ID
     * @param string $login
     * @param int $gradeId
     */
    public function __construct($ID, $login, $gradeId)
    {
        $this->ID      = $ID;
        $this->login   = $login;
        $this->gradeId = $gradeId;
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO User (login, gradeId)
        VALUES ('$this->login', '$this->gradeId');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE User
        SET login='$this->login', gradeId='$this->gradeId'
        WHERE ID = '$this->ID';";

    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM User WHERE ID = '$this->ID';";
    }
}