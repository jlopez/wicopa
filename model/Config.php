<?php 

require_once(__DIR__.'/../dao/DBquery.php');
require_once(__DIR__.'/../model/Model.php');

/**
 * Class Config
 */
class Config extends Model
{
    var $ID          = 0;
    var $type        = "";
    var $description = "";
    var $value       = "";
    var $active      = 0;

    /**
     * Config constructor
     * 
     * @param int $ID
     * @param $string $type
     * @param string $description
     * @param string $value
     * @param int $active
     */
    public function __construct($ID, $type, $description, $value, $active)
    {
        $this->ID          = $ID;
        $this->description = $description;
        $this->value       = $value;
        $this->type        = $type;
        $this->active      = $active;
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
        $this->description = utf8_decode($db->escape($this->description));
        $this->value = utf8_decode($db->escape($this->value));
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO Config (type, description, value, active)
        VALUES ('$this->type', '$this->description', '$this->value', '$this->active');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE Config
        SET type='$this->type', description='$this->description', value='$this->value', active='$this->active'
        WHERE ID = '$this->ID';";
    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM Config WHERE ID = '$this->ID';";
    }



}