<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 11/04/19
 * Time: 10:34
 */
require_once(__DIR__.'/../model/Model.php');

 /**
  * Class ContainerLabel
  */
 class ContainerLabel extends Model
 {
    var $ID          = 0;
    var $containerId = 0;
    var $labelId     = 0; 
 

    /**
     * @param int $ID
     * @param int $containerId
     * @param int $labelId
     */
    public function __construct($ID, $containerId, $labelId)
    {
        $this->ID          = $ID;
        $this->containerId = $containerId;
        $this->labelId     = $labelId;
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO ContainerLabel (containerId, labelId)
        VALUES ('$this->containerId', '$this->labelId');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE ContainerLabel
        SET containerId='$this->containerId', labelId='$this->labelId'
        WHERE ID = '$this->ID';";
    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM ContainerLabel WHERE ID = '$this->ID';";
    }

}