<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 27/02/19
 * Time: 13:46
 */

require_once(__DIR__.'/../model/Model.php');

/**
 * Class Distribution
 */
class Distribution extends Model
{

    var $ID      = -1;
    var $name    = "";
    var $version = "";
    var $manager = "";
    var $active  = 0;
    var $state   = 0;
    var $vname   = "";

    /**
     * Section constructor.
     * @param int $ID
     * @param string $name
     * @param string $version
     * @param string $manager
     * @param int $active
     * @param int $state
     * @param string $vname
     */
    public function __construct($ID, $name, $version, $manager, $active, $vname)
    {
        $this->ID      = $ID;
        $this->name    = $name;
        $this->version = $version;
        $this->manager = $manager;
        $this->active  = $active;
        $this->vname   = $vname;
        $this->state = 0;
    }
    
    /**
     * Get Full name for this Distribution
     */
    public function getFullName() 
    {
        return $this->name . ":" . $this->version;
    }

    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO Distrib (name, version, manager, active, vname, state)
        VALUES ('$this->name', '$this->version', '$this->manager', '$this->active', '$this->vname', '$this->state');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE Distrib
        SET name='$this->name', version='$this->version', manager='$this->manager', active=$this->active, vname='$this->vname', state='$this->state'
        WHERE ID = $this->ID;";

    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM Distrib WHERE ID = $this->ID;";
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
        $this->name  = utf8_decode($db->escape($this->name));
        $this->version = utf8_decode($db->escape($this->version));
        $this->manager = utf8_decode($db->escape($this->manager));
        $this->vname = utf8_decode($db->escape($this->vname));
    }

    /**
     * Activate update for this distribution
     */
    public function activateUpdate() {
        $this->state = 1;
    }

    /**
     * Deactivate update for this distribution
     */
    public function deactivateUpdate() {
        $this->state = 0;
    }

}