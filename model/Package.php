<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 08/04/19
 * Time: 14:16
 */

require_once(__DIR__.'/../dao/DBquery.php');
require_once(__DIR__.'/../model/Model.php');

/**
 * Class Package
 */
class Package extends Model
{

    var $name        = "";
    var $version     = "";
    var $description = "";
    var $doc         = "";
    var $other       = "";

    /**
     * Package constructor.
     * @param string $name
     * @param string $active
     * @param string $color
     * @param string $arrangement
     * @param string $parent
     */
    public function __construct($name, $version, $description, $doc, $other)
    {
        $this->name        = $name;
        $this->version     = $version;
        $this->description = $description;
        $this->doc         = $doc;
        $this->other       = $other;
    }


}