<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 11/04/19
 * Time: 10:21
 */

require_once(__DIR__.'/../dao/DBquery.php');
require_once(__DIR__.'/../dao/DBquery.php');

/**
 * Class Container
 */
class Container extends Model
{
    var $ID          = 0;
    var $name        = "";
    var $value       = "";
    var $type        = "";
    var $visibility  = 1;
    var $description = "";
    var $author      = 0;
    var $date        = NULL;
    var $tags        = "";

    /**
     * Container constructor.
     * @param int $ID
     * @param string $name
     * @param string $value
     * @param string $type
     * @param int $visibility
     * @param string $description
     * @param int $author
     * @param date $date
     * @param string $tags
     */
    public function __construct($ID, $name, $value, $type, $visibility, $description, $author, $date, $tags)
    {
        $this->ID          = $ID;
        $this->name        = $name;
        $this->value       = $value;
        $this->type        = $type;
        $this->visibility  = $visibility;
        $this->description = $description;
        $this->author      = $author;
        $this->date        = $date;
        $this->tags        = $tags;
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape(DBquery $db) {
        $this->name        = utf8_decode($db->escape($this->name));
        $this->value   = utf8_decode($db->escape($this->value));
        $this->description = utf8_decode($db->escape($this->description));
        $this->tags = utf8_decode($db->escape($this->tags));
    }

    /**
     * Escape field
     * 
     * @param DBquery $db The database object
     */
    public function escape2(DBquery $db) {
        $this->name        = utf8_encode($db->escape($this->name));
        $this->value   = utf8_encode($db->escape($this->value));
        $this->description = utf8_encode($db->escape($this->description));
        $this->tags = utf8_encode($db->escape($this->tags));
    }
    
    /**
     * Get insert sql value
     * 
     * @return string
     */
    public function getInsert() {
        return "INSERT INTO Container (name, value, type, visibility, description, author, date, tags)
        VALUES ('$this->name', '$this->value', '$this->type', '$this->visibility', '$this->description', '$this->author', '$this->date', '$this->tags');";
    }

    /**
     * Get update sql value
     * 
     * @return string
     */
    public function getUpdate() {
        return "UPDATE Container
        SET name='$this->name', value='$this->value', type='$this->type', 
        visiblity='$this->visibility', description='$this->description', author='$this->author', date='$this->date', tags='$this->tags'
        WHERE ID = '$this->ID';";

    }

    /**
     * Get delete sql value
     * 
     * @return string
     */
    public function getDelete() {
        return "DELETE FROM Container WHERE ID = '$this->ID';";
    }

}