<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 01/02/19
 * Time: 16:26
 */


require_once "./dao/DBquery.php";
require_once "./package.php";

$db = new DBquery();

$distrib = "";
$save_distrib = "";

if(isset($_GET['distrib'])) {
    $distrib = $_GET['distrib'];
    $save_distrib = $_GET['distrib'];
}

$filter = "";

if(isset($_GET['filter'])) {
    $filter = $_GET['filter'];
}

if(isset($_GET['filter'])) {
    $filter = $_GET['filter'];
}

$patternName = true;
$patternDescription = true;

/*
if(isset($_GET['patternName'])) {
    if($_GET['patternName'] == "true") {
        $patternName = true;
    }
}

if(isset($_GET['patternDescription'])) {
    if($_GET['patternDescription'] == "true") {
        $patternDescription = true;
    }
}*/

if(isset($_GET['limit'])) {
    $limit = $_GET['limit'];
}

$result = array();
$version_num  = "";
$version_name = "";

if(strpos($distrib, ':') !== false) {
    $os = explode (":", $distrib);
    $os_name = $os[0];
    $version_num = $os[1];
    $version_name = $db->getVName($os_name, $version_num);
    $result = findDistributionPackage($os_name, $version_num, $version_name, $filter, $limit, false);
} else {

    if($distrib == "Conda") {
        $result = findCondaPackage($distrib, $version_num, $version_name, $filter, $limit, false);
    }
    else if($distrib == "Rubygems") {
        $result = findRubyPackage($distrib, $version_num, $version_name, $filter, $limit, false);
    } 
    else if($distrib == "NPM") {
       $result = findNPMPackage($distrib, $version_num, $version_name, $filter, $limit, false);
    }
    else if($distrib == "CPAN") {
        $result = findCPANPackage($distrib, $version_num, $version_name, $filter, $limit, false);
    }
    else if($distrib == "CRAN" || $distrib == "Bioconductor") {
        $result = findRPackage($distrib, $version_num, $version_name, $filter, $limit, false);
    }
    else if($distrib == "Python") {
        $result = findPythonPackage($distrib, $version_num, $version_name, $filter, $limit, false);
    }
    else {
        $result = $db->getPackages($distrib, $filter, $patternName, $patternDescription, $limit);
    }
}

echo '
<div class="container">
  <div class="row justify-content-center">
    <h4>Packages found : '.count($result).'</h4>
  </div>
</div>


<table class="table table-striped table-bordered">
    <thead>
            <tr>
                <th>Choose</th>
                <th>Name</th>
                <th>Version</th>
                <th>Description</th>
                <th>Documentation</th>
                <th>Other</th>                             
            </tr>
    </thead>
    <tbody>
        
';

if(strpos($distrib, ':') !== false) {
    $distrib = "Distribution";
}

foreach ($result as $t) {

    echo '<tr>
        <td>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" onclick="generatePackageSelected(this)" data-pname="'.$t->name.'" data-pversion="'.$t->version.'"  data-psection="'.$distrib.'" id="checkbox$'.$distrib.'$'.$t->name.'$'.$t->version.'" class="custom-control-input" name="type">                                         
                <span class="custom-control-indicator"></span>
            </label>
        </td>';
    echo '<td>'.$t->name.'</td>';
    echo '<td>'.$t->version.'</td>';
    echo '<td>'.$t->description.'</td>';

    if($t->doc) {
        if($t->doc != "") {
            echo '<td><a href="'.$t->doc.'">documentation</a></td>';
        } else {
            echo '<td></td>';
        }
    } else {
        echo '<td></td>';
    }

    if($distrib == "Distribution") {
        echo '<td>'.$save_distrib.'</td>';
    } else {
        echo '<td>'.$t->other.'</td>';
    }
    
   
    echo '</tr>';
}

echo '</tr>
    </tbody>
</table>
';
