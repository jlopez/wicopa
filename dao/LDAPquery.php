<?php

require_once(__DIR__.'/../inc/conf/Conf.php');

/**
 * Class LDAPquery for connection to the LDAP
 */
class LDAPquery {

    var $ldaph = NULL;

    /**
     * Constructor
     */
    public function __construct() {
        $this->openConnection();
    }

    /**
     * Open connection with LDAP
     */
    public function openConnection() {
        $this->ldaph = ldap_connect(Conf::$LDAP_HOSTNAME);

        if(!$this->ldaph) {
            die("Impossible de se connecter au serveur LDAP ".Conf::$LDAP_HOSTNAME);
        }
    }

    /**
     * Bind with LDAP
     */
    public function bind() {
        ldap_set_option($this->ldaph, LDAP_OPT_PROTOCOL_VERSION, 3);
        $r = ldap_bind($this->ldaph, Conf::$LDAP_ADMIN, Conf::$LDAP_PP);
        return $r;
    }

    /**
     * Verify user login and password with LDAP for user connection
     */
    public function verifyPass($ul, $up) {
        $r = $this->bind();
        if($r) {
            $sr=ldap_search($this->ldaph, Conf::$LDAP_USER, "(cn=*)");
            $info = ldap_get_entries($this->ldaph, $sr);
            for ($i=0; $i<$info["count"]; $i++) {

                $login = $info[$i]["cn"][0];

                if($login == $ul) {
                    if($info[$i]["userpassword"][0] == Conf::ldapEncodePass($up)) {
                        return 1;
                    };
                }
            }
        }

        return 0;
    }

}
