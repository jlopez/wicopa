#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
from os import walk
import os
from bs4 import BeautifulSoup
from rpmUtils.miscutils import splitFilename
import sys
reload(sys)
sys.setdefaultencoding('utf8')

class Packages:
    def __init__(self, name, version, description, doc):
        self.name = name
        self.version = version
        self.description = description
        self.doc = doc
        self.rpm = ""

    def to_string(self, distrib_os, distrib_release):

        self.description = self.description.replace('"','\'')
        
        #if(distrib_os == "centos" or distrib_os == "fedora"):
        #    return "\"" + self.name + "\";\"" + self.version + "\";\"" + self.description + "\";\"" + self.doc + "\";\"" + distrib_os + "\";\"" + distrib_release + "\";\"" + self.rpm + "\"\n"
        #else:
        #    return "\"" + self.name + "\";\"" + self.version + "\";\"" + self.description + "\";\"" + self.doc + "\";\"" + distrib_os + "\";\"" + distrib_release + "\"\n"

        if(distrib_os == "centos" or distrib_os == "fedora"):
            return "\"" + self.name + "\";\"" + self.version + "\";\"" + self.description + "\";\"" + self.doc + "\";\"" + self.rpm + "\"\n"
        else:
            return "\"" + self.name + "\";\"" + self.version + "\";\"" + self.description + "\";\"" + self.doc + "\";\"" + "\"\n"


class Distrib:
    def __init__(self, name, version_num, version_name, url_packages):
        self.name = name
        self.version_num = version_num
        self.version_name = version_name
        self.url_packages = url_packages
        self.packages = []

    

    def getPackages(self):
        name_dir = "./"+self.name+":"+self.version_num
        if(not os.path.isdir(name_dir)):
            os.mkdir(name_dir)

        if(os.path.exists(name_dir+"/packages.html")):
                os.remove(name_dir+"/packages.html")


        url = name_dir+"/packages.html"

        if(self.name == "alpine"):

            cont_download = True
            indexPage = 1


            while cont_download :

                url_page = self.url_packages + "?page=" + str(indexPage) +"&branch=v"+self.version_num+"&arch=x86_64"
                os.system("cd " + name_dir + "; wget -q -c '" + url_page + "' -O " + str(indexPage)+".html")
                path_file = name_dir + "/" + str(indexPage) + ".html" 

                if os.stat(path_file).st_size <= 18552: #TODO fixe du pauvre
                    cont_download = False
                    indexPage = indexPage - 1
                else:
                    indexPage = indexPage + 1


            print("Start : " + self.name + "-" + self.version_name)

            results = open(name_dir+"/packages.csv","w")

            indexP = 0
            indexIMG = 0

            for i in range(1,indexPage) :

                path_file = name_dir + "/" + str(i) + ".html" 


                soup = BeautifulSoup(open(path_file), "html.parser")

                indexIMG = 0

                for tr in soup.find_all('tr'):

                    if indexIMG > 0:
                        
                        children = tr.findChildren()
                        name = children[1].get_text()
                        version = children[2].get_text()
                        currentP = Packages(name, version, "", "")
                        currentP.doc = "https://pkgs.alpinelinux.org/package/v"+ self.version_num + "/main/x86_64/" + name
                        results.write(currentP.to_string(self.name, self.version_num).encode('utf-8'))
                        indexP = indexP + 1
              
                           
                    indexIMG = indexIMG + 1

            print("Packages = " + str(indexP))
            print("End : " + self.name + "-" + self.version_name)

            results.close()

        elif(self.name == "fedora"):

            pk_tab = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

            for pk in pk_tab:
                os.system("cd " + name_dir + "; wget -q -c " + self.url_packages + pk + "/ -O " + pk+".html")

            print("Start : " + self.name + "-" + self.version_name)

            results = open(name_dir+"/packages.csv","w")

            indexP = 0
            indexIMG = 0


            for pk in pk_tab:

                path_file = name_dir + "/" + pk + ".html" 

                if os.stat(path_file).st_size > 0:
                    
                    indexIMG = 0

                    soup = BeautifulSoup(open(path_file), "html.parser")

                    for img in soup.find_all('a'):

                        if indexIMG > 4:
                            values = img.get_text()                     
                            (n, v, r, e, a) = splitFilename(values)
                            currentP = Packages(n, v, "", "")
                            currentP.rpm = values
                            currentP.doc = "https://dl.fedoraproject.org/pub/fedora/linux/releases/"+ self.version_num + "/Everything/source/tree/Packages/" + pk + "/" + values
                            results.write(currentP.to_string(self.name, self.version_num).encode('utf-8'))
                            indexP = indexP + 1
                   
                            
                        indexIMG = indexIMG + 1
                        
                    
             

            print("Packages = " + str(indexP))
            print("End : " + self.name + "-" + self.version_name)

            results.close()

              


        elif(self.name == "centos"):
            os.system("cd " + name_dir + "; wget -q -c " + self.url_packages + " -O packages.html")


            soup = BeautifulSoup(open(url), "html.parser")

            results = open(name_dir+"/packages.csv","w")

            print("Start : " + self.name + "-" + self.version_name)

            indexP = 0
            indexTR = 0


            for tr in soup.find_all('tr'):

                if indexTR > 4:

                    children = tr.findChildren()

                    if len(children) > 2:

                        values = children[2].get_text() 
            
                        if "x86_64" in values: 
                            (n, v, r, e, a) = splitFilename(values)
                            currentP = Packages(n, v, "", "")
                            currentP.rpm = values
                            currentP.doc = "http://mirror.centos.org/centos/"+ self.version_num + "/os/x86_64/Packages/" + values
                            results.write(currentP.to_string(self.name, self.version_num).encode('utf-8'))
                            indexP = indexP + 1
                       
                indexTR = indexTR + 1

            print("Packages = " + str(indexP))
            print("End : " + self.name + "-" + self.version_name)

            results.close()


        elif(self.name == "ubuntu" or self.name == "debian"):

            os.system("cd " + name_dir + "; wget -q -c " + self.url_packages + " -O packages.html")

            
            soup = BeautifulSoup(open(url), "html.parser")

            results = open(name_dir+"/packages.csv","w")
            
            print("Start : " + self.name + "-" + self.version_name)

            indexP = 0

            for dl in soup.find_all('dl'):
                children = dl.findChildren()
                indexP = 0
                currentP = None
                currentID  = 0
                for child in children:
                    if child.name == "dt":
                        
                        values = child.get_text().split(" ")

                        vv = 2

                        if(self.name == "ubuntu"):
                            vv = 3

                        if(len(values) >= vv):
                            indexP = indexP + 1
                            name = values[0]
                            version = values[1][1:-1]
                            currentP = Packages(name, version, "", "")
                            currentID = indexP
                        else:
                            currentID = -1
                    
                    elif child.name == "dd":
                        description = child.get_text()
                        if(currentID > -1):
                            currentP.description = description
                            currentP.doc = "https://packages.ubuntu.com/" + self.version_name + "/" + currentP.name + "/"
                            #print(currentP.to_string(self.name, self.version_num))
                            results.write(currentP.to_string(self.name, self.version_num).encode('utf-8'))
                            
            print("Packages = " + str(indexP))
            print("End : " + self.name + "-" + self.version_name)

            results.close()

            

        else:
            print("This OS not yet implemented")

if __name__ == '__main__':

    print("Start");

    allS = []
    #allS.append(Distrib("ubuntu", "16.04", "xenial", "https://packages.ubuntu.com/xenial/allpackages"))
    #allS.append(Distrib("ubuntu", "18.04", "bionic", "https://packages.ubuntu.com/bionic/allpackages"))
    #allS.append(Distrib("debian", "8", "jessie", "https://packages.debian.org/jessie/allpackages"))
    #allS.append(Distrib("debian", "9", "stretch", "https://packages.debian.org/stretch/allpackages"))
    allS.append(Distrib("centos", "7", "7", "http://mirror.centos.org/centos/7/os/x86_64/Packages/"))
    allS.append(Distrib("fedora", "28", "28", "https://dl.fedoraproject.org/pub/fedora/linux/releases/28/Everything/source/tree/Packages/"))
    #allS.append(Distrib("alpine", "3.9", "3.9", "https://pkgs.alpinelinux.org/packages"))
    
    for d in allS:
        d.getPackages()

    print("End");
