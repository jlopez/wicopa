from xmlrpclib import ServerProxy
import re
from os import walk
import os
import sys
from bs4 import BeautifulSoup
from rpmUtils.miscutils import splitFilename

import urllib, json

reload(sys)
sys.setdefaultencoding('utf8')


URL_INFO = 'https://pypi.python.org/pypi/'
URL_SIMPLE = "https://pypi.org/simple/"

def run():

    try:
        import xmlrpclib
    except ImportError:
        import xmlrpc.client as xmlrpclib

    client = xmlrpclib.ServerProxy('https://pypi.python.org/pypi')
    # get a list of package names
    packages = client.list_packages()

    indexP = 0

    print("Start : python")

    results = open("./python/packages.csv","w")

    for p in packages:

        print(p)
        
        response = urllib.urlopen(URL_INFO+p+"/json")

        if response.getcode() == 404 :
            print(" 404 : " + p)
            continue

        dataP = json.loads(response.read())
    
        name = p

        version = dataP['info']['version']
        description = dataP['info']['summary']
        doc = dataP['info']['home_page']

        if(doc == None):
           doc = "" 
        

        classifiers = dataP['info']['classifiers']

        depends_python = ""
        for c in classifiers:
            if("Programming Language" in c):
                #print(c)
                val1 = c.replace('Programming Language :: Python','')
                if val1 != "":
                    val2 = val1.replace(' :: ', '')
                    depends_python = depends_python + val2 + ' | '

        if(depends_python == ""):
            depends_python = "all"

        if(version == None):
            version = "0"

        if(description == None):
            description = ""
        
        if(doc == None):
            doc = ""

        line = '"' + name + '";"' + version + '";"' + description + '";"' + doc + '";"' + depends_python + '"\n'

        line = line.encode('utf-8')

        results.write(line)

        

        indexP = indexP + 1

        print(str(indexP) + " / " + str(len(packages)))



 

    
    print("Packages : " + str(indexP))
    print("End : python")


if __name__ == '__main__':

    run()
