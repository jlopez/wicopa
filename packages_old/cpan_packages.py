#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import walk
import os
import re

if __name__ == '__main__':

    print("Start");

    results = open("./cpan/packages.csv","w")

    indexP = 0

    with open("./cpan/packages.txt", "r") as ins:
        for line in ins:
            nl = ' '.join(line.split()).replace(' ', ';')

            nt = nl.split(";")

            tmpI = 0
            res = ""
            for n in nt:
                if tmpI == 0:
                    res = res + '"' + n + '"'
                else:
                    res = res + '; "' + n + '"'

                tmpI += 1

            res = res + '; ""'

            results.write(nl+"\n")
            indexP = indexP + 1

    results.close()

    print("Packages = " + str(indexP))

    print("End");