#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import string
import subprocess
import json

SHORT_DOWNLOAD = False

if __name__ == '__main__':

    print("Start ");

    alp = list(string.ascii_lowercase)

    indexP = 0
    
    if SHORT_DOWNLOAD :
        results = open("./ruby/packages.csv","w")
    else:
        results = open("./ruby/packages-full.csv","w")

    for i in alp:
        cmd = " gem search ^"+i+" > ./ruby/"+i+".txt"
        os.system(cmd)

        print(i)

        with open("./ruby/"+i+".txt" , "r") as ins:

            for line in ins:
                values = line.split()
                name = values[0]

                if SHORT_DOWNLOAD :

                    depends = values[1].split()[0]
                    version = depends[1:]

                    if(version[len(version)-1:len(version)] != ")"):
                        pass
                    else:
                        version = version[:-1]

                    doc = "http://www.rubydoc.info/gems/"+name+"/"+version


                    res = '"' + name + '";"' + version + '";"";"' +  doc + '"\n'

                else:

                    subcmd = "curl -s https://rubygems.org/api/v1/gems/"+name+".json"
                    print(subcmd)
                    output = subprocess.check_output(subcmd, shell=True)

                    parsed_json = json.loads(output)

                    res = '"' + parsed_json['name'] + '";"' + parsed_json['version'] + '";"' \
                        + parsed_json['info'] + '";"' \
                        + parsed_json['documentation_uri'] + '"' + "\n"
                    print(res)

                indexP = indexP + 1
                results.write(res.encode('utf-8'))

    results.close()

    print("Packages : " + str(indexP))
    print("End");
