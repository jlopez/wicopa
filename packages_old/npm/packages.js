const registry = require('all-the-packages')

var fs = require('fs');
var stream = fs.createWriteStream("./packages.csv");

console.log("Start");

var index = 0;

stream.once('open', function(fd) {

registry
  .on('package', function (pkg) {
          //console.log(`${pkg.name} - ${pkg.description}\n`)
        index++;
        var name = pkg.name;
        var version = pkg.version;
        var description = pkg.description;
        var doc = "https://www.npmjs.com/package/"+name;
        var line = '"' + name + '";"' + version + '";"' + description + '";"' + doc + '"\n';
        //console.log(line);
        stream.write(line);
        console.log(index);
      })
  .on('end', function () {
          // done
    })

// stream.end();
});


console.log(index);
console.log("End");
