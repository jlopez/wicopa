#!/usr/bin/env python
# -*- coding: utf-8 -*-



import json

# wget https://repo.anaconda.com/pkgs/main/linux-64/repodata.json

from pprint import pprint

import os
import sys

if __name__ == '__main__':

    print("Start")

    with open('./anaconda/repodata.json') as f:
        data = json.load(f)

    packages = data['packages']



    results = open("./anaconda/packages.csv","w")

    for p in packages:
        pk = data['packages'][p]
        line = '"'+data['packages'][p]['name']+'"'+";"+'"'+data['packages'][p]['version']+'"'+";"+'""'+";"+"https://anaconda.org/anaconda/"+data['packages'][p]['name']+"\n"
        results.write(line)

    results.close()

    print("End")

