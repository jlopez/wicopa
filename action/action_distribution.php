<?php

define("PATH","..");
define("PAGE","actionDistribution");

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ../login.php");
}

require_once '../dao/DBquery.php';

$db = new DBquery();

$today = date("Y-m-d G:i:s");

$action  = NULL;

$id      = NULL;
$version  = NULL;
$manager  = NULL;
$active   = 0;
$vname = "";


if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['distribid'])) {
    $id = $_POST['distribid'];
} else {
    if(isset($_GET['distribid'])) {
        $id = $_GET['distribid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['version'])) {
    $version = $_POST['version'];
}

if(isset($_POST['manager'])) {
    $manager = $_POST['manager'];
}

if(isset($_POST['active'])) {

    if($_POST['active'] == $id) {
        $active = 1;
    } else {
        $active = 0;
    }
}

if(isset($_POST['vname'])) {
    $vname = $_POST['vname'];
}

$distrib = new Distribution($id, $name, $version, $manager, $active, $vname);

if($action == "create") {
    $distrib->active = 1;
    $distrib->escape($db);
    $db->create($distrib);
} else if ($action == "update") {
    $distrib->escape($db);
    $db->update($distrib);
} else if ($action == "delete") {
    $db->delete($distrib);
}

header("Location: ../manage.php#distribution");

