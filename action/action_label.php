<?php

define("PATH","..");
define("PAGE","actionLabel");

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ../login.php");
}



require_once '../dao/DBquery.php';

$db = new DBquery();



$action = NULL;

$id          = NULL;
$name        = NULL;
$color       = NULL;
$gradeId     = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['labelid'])) {
    $id = $_POST['labelid'];
} else {
    if(isset($_GET['labelid'])) {
        $id = $_GET['labelid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['color'])) {
    $color = $_POST['color'];
}

if(isset($_POST['gradeId'])) {
    $gradeId = $_POST['gradeId'];
}

$label = new Label($id, $name, $color, $gradeId);

if($action == "create") {
    $label->escape($db);
    $db->create($label);
} else if ($action == "update") {
    $label->escape($db);
    $db->update($label);
} else if ($action == "delete") {
    $db->delete($label);
}

header("Location: ../manage.php#label");
