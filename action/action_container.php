<?php

define("PATH","..");
define("PAGE","actionContainer");

session_start ();

$action = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(!isset($_SESSION['username'])) {
    if($action != "download") {
        header("Location: ../login.php");
    }
}

require_once '../dao/DBquery.php';

$db = new DBquery();

$today = date("Y-m-d G:i:s");


$id          = NULL;
$name        = NULL;
$value       = NULL;
$type        = NULL;
$visibility  = "public";
$description = NULL;
$author      = 0;
$labels = array();
$tags = "";

if(isset($_POST['containerid'])) {
    $id = $_POST['containerid'];
} else {
    if(isset($_GET['containerid'])) {
        $id = $_GET['containerid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['value'])) {
    $value = $_POST['value'];
}

if(isset($_POST['type'])) {
    $type = $_POST['type'];
}

if(isset($_POST['visibility'])) {
    $visibility = intval($_POST['visibility']);
}

if(isset($_POST['description'])) {
    $description = $_POST['description'];
}

$author = $_SESSION['username'];

if(isset($_POST['labels'])) {
    $labels = $_POST['labels'];
}

if(isset($_POST['tags'])) {
    $tags = $_POST['tags'];
    $tags = preg_replace("/[\n\r]/","",$tags); 
    $tags = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $tags);
}

$container = new Container($id, $name, $value, $type, $visibility, $description, $author, $today, $tags);

if($action == "create") {
    $container->escape($db);
    $db->create($container);

    $last_id = mysqli_insert_id($db->dbh);

    foreach ($labels as $lid){
        $contlab = new ContainerLabel(0, $last_id, $lid);
        $db->create($contlab);
    }

    header("Location: ../container.php");
    
} else if ($action == "update") {
    $container->escape($db);
    $db->update($container);

    header("Location: ../container.php");
} else if ($action == "delete") {

    $cnt = $db->getContainerWithId($container->ID);

    if($cnt->author == $author) {
        $db->delete($container);
    } else if($db->getGradeWithLogin($_SESSION['username'])->name == $db->getMaxGrades()->name) {
        $db->delete($container);
    }

    header("Location: ../container.php");
    
} else if($action == "download") {

    $cnt = $db->getContainerWithId($container->ID);

    $down = FALSE;
    if(($cnt->author == $author) || ($cnt->visibility == 1)) {
        $down = TRUE;
    } else if($db->getGradeWithLogin($_SESSION['username'])->name == $db->getMaxGrades()->name) {
        $down = TRUE;
    }

    if($down) {
        $content = $cnt->value;
        $name = $cnt->name;

        $file = fopen($name,"wb");
        fwrite($file);
        fclose($file);

        header('Content-Type: charset=utf-8');
        header("Content-disposition: attachment; filename=$name");

        print $content;
    } else {
        header("Location: ../container.php");
    }
} else {
    header("Location: ../container.php");
}


