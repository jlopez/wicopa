<?php

define("PATH","..");
define("PAGE","actionUser");

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ../login.php");
}

require_once '../dao/DBquery.php';

$db = new DBquery();

$action = NULL;

$id      = NULL;
$login   = NULL;
$gradeId = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['userid'])) {
    $id = $_POST['userid'];
} else {
    if(isset($_GET['userid'])) {
        $id = $_GET['userid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['login'])) {
    $login = $_POST['login'];
}

if(isset($_POST['gradeId'])) {
    $gradeId = $_POST['gradeId'];
}

$user = new User($id, $login, $gradeId);

if($action == "create") {
    $user->escape($db);
    $db->create($user);
} else if ($action == "update") {
    $user->escape($db);
    $db->update($user);
} else if ($action == "delete") {
    $db->delete($user);
}

header("Location: ../manage.php#user");
