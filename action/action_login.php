<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 26/02/19
 * Time: 13:01
 */

session_start ();

require_once '../dao/DBquery.php';

require_once '../dao/LDAPquery.php';

$db = new DBquery();

session_start ();

$today = date("Y-m-d G:i:s");

$login = $db->escape($_POST['username']);
$pwd   = $db->escape($_POST['password']);

$passcode = $db->verifyPass($login, $pwd);

if($passcode == 1) {
    $_SESSION['username'] = $login;
    $_SESSION['usercode'] = 1;
    $_SESSION['manage'] = 1;
    header("Location: ../manage.php");

} elseif ($passcode == 2) {
    $_SESSION['username'] = "guest";
    $_SESSION['usercode'] = 2;
    $_SESSION['manage'] = 1;
    header("Location: ../manage.php");
} else {

    $ldap = new LDAPquery();

    $ldappasscode = $ldap->verifyPass($login, $pwd);

    if($ldappasscode == 1) {
        $_SESSION['username'] = $login;
        $_SESSION['usercode'] = 3;

        if($db->getGradeWithLogin($_SESSION['username'])->name == $db->getMaxGrades()->name) {
            $_SESSION['manage'] = 1;
        }
        
        header("Location: ../index.php");
    } else {
       header("Location: ../login.php");
    }
}
