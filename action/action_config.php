<?php

define("PATH","..");
define("PAGE","actionConfig");

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ../login.php");
}

require_once '../dao/DBquery.php';

$db = new DBquery();

$today = date("Y-m-d G:i:s");

$action  = NULL;

$id          = NULL;
$type        = NULL;
$description = NULL;
$value       = NULL;
$active      = 0;


if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['configid'])) {
    $id = $_POST['configid'];
} else {
    if(isset($_GET['configid'])) {
        $id = $_GET['configid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['type'])) {
    $type = $_POST['type'];
}

if(isset($_POST['description'])) {
    $description = $_POST['description'];
}

if(isset($_POST['value'])) {
    $value = $_POST['value'];
}

if(isset($_POST['active'])) {
    $active = 1;
}

$config = new Config($id, $type, $description, $value, $active);

if($action == "create") {
    $config->active = 1;
    $config->escape($db);
    $db->create($config);
} else if ($action == "update") {
    $config->escape($db);
    $db->update($config);
} else if ($action == "delete") {
    $db->delete($config);
}


header("Location: ../configuration.php");