<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 26/02/19
 * Time: 13:08
 */

session_start ();

session_destroy();

header("Location: ../index.php");
