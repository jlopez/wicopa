<?php

define("PATH","..");
define("PAGE","actionManager");

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ../login.php");
}

require_once '../dao/DBquery.php';


$db = new DBquery();

$today = date("Y-m-d G:i:s");

$action  = NULL;

$id      = NULL;
$value   = NULL;


if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['managerid'])) {
    $id = $_POST['managerid'];
} else {
    if(isset($_GET['managerid'])) {
        $id = $_GET['managerid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['value'])) {
    $value = $_POST['value'];
}

$manager = new Manager($id, $name, $value);

if($action == "create") {
    $manager->escape($db);
    $db->create($manager);
} else if ($action == "update") {
    $manager->escape($db);
    $db->update($manager);
} else if ($action == "delete") {
    $db->delete($manager);
}

header("Location: ../manage.php#manager");