<?php

define("PATH","..");
define("PAGE","actionSection");

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ../login.php");
}

require_once '../dao/DBquery.php';

$db = new DBquery();

$today = date("Y-m-d G:i:s");

$action  = NULL;

$id          = NULL;
$name        = NULL;
$visual      = NULL;
$active      = 0;
$color       = NULL;
$arrangement = NULL;



if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['sectionid'])) {
    $id = $_POST['sectionid'];
} else {
    if(isset($_GET['sectionid'])) {
        $id = $_GET['sectionid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['visual'])) {
    $visual = $_POST['visual'];
}

if(isset($_POST['color'])) {
    $color = "#".$_POST['color'];
}

if(isset($_POST['arrangement'])) {
    $arrangement = $_POST['arrangement'];
}


if(isset($_POST['active'])) {

    if($_POST['active'] == $id) {
        $active = 1;
    } else {
        $active = 0;
    }
}

$section = new Section($id, $name, $visual, $active, $color, $arrangement, "");

if($action == "create") {
    $section->active = 1;
    $section->escape($db);
    $db->create($section);
} else if ($action == "update") {
    $section->escape($db);
    $db->update($section);
} else if ($action == "delete") {
    $db->delete($section);
}

header("Location: ../manage.php#section");

