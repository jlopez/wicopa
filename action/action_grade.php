<?php

define("PATH","..");
define("PAGE","actionGrade");

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ../login.php");
}

require_once '../dao/DBquery.php';

$db = new DBquery();

$action = NULL;

$id          = NULL;
$name        = NULL;
$level       = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['gradeid'])) {
    $id = $_POST['gradeid'];
} else {
    if(isset($_GET['gradeid'])) {
        $id = $_GET['gradeid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['level'])) {
    $level = $_POST['level'];
}

$grade = new Grade($id, $name, $level);

if($action == "create") {
    $grade->escape($db);
    $db->create($grade);
} else if ($action == "update") {
    $grade->escape($db);
    $db->update($grade);
} else if ($action == "delete") {
    $db->delete($grade);
}

header("Location: ../manage.php#grade");

