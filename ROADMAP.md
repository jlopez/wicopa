** ROADMAP ideas **

  - Recode in PHP the update codes for package managers / issue [#2](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/2),
  - Adding a "local field", with checkbox on the user panel to allow a local admin to add a custom field on each recipe (optionnal for the user) (see [#7](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/7))
  - Create a session / user table. Connect and bind to LDAP [#9](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/9),
  - Create an history by user session [#10](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/10),
  - Give the possibility to users to share their recipes (when these recipes are validated ?) by adding some other tags (docker's metadatas, singularity's LABELs) [#10](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/10) and [#11](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/11),
  - Give the possibility to browse the recipes shared using keywords  [#11](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/11),
  - Give the possibility to launchs Diffs between recipes,
  - Code import from Dockerhub, quay.io, SingularityHub... / issue [#4](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/4),
