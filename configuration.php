<?php

session_start ();

if(!isset($_SESSION['manage'])) {
    header("Location: ./login.php");
}

require_once "./dao/DBquery.php";

$db = new DBquery();

$configs = $db->getConfigs();


require_once "./inc/php/buildHeader.php";

?>

<div class="container-fluid">

    <br/>

    <div class="row">

    <div class="col-12">

        <div class="card border border-dark">
            <div class="card-header bg-dark text-light">
                Configs :
            </div>
            <div class="card-body">

                <form action="./action/action_config.php" method="post">
                    <div  class="row">
                    <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                        <div class="col-2">
                            <label for="typeConfig">Type :</label>
                            <select name="type" id="typeConfig" class="form-control">';
                                <option value="post">post</option>
                                <option value="env">env</option>
                            </select>
                        </div>
                        <div class="col-3">
                            <label for="descriptionConfig">Description :</label>
                            <input type="text" class="form-control" id="descriptionConfig" name="description" >
                        </div>
                        <div class="col-4">
                            <label for="valueConfig">Value :</label>
                            <textarea class="form-control" id="valueConfig" rows="5" name="value" maxlength="999" ></textarea>
                        </div>
                        <div class="col-3">
                           
                        <button type="submit" class="btn btn-success">Create</button>
                        </div>
                    </div>


                </form>

                <br/>
                <br/>
                <br/>

                <table id="Table_Config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Active</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Value</th>
                            <th>Save</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody id="TableConfig">

                    <?php 

                    foreach ($configs as $config) {
                        echo '<form action="./action/action_config.php" method="post"><tr>';
                        echo '<td>
                            <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="update">
                            <input type="text" style="display: none;" name="configid" value="'.$config->ID.'">
                            ';

                            if($config->active == 1) {
                                echo '<input type="checkbox" name="active" value="'.$config->ID.'" checked>';
                            } else {
                                echo '<input type="checkbox" name="active" value="'.$config->ID.'">';
                            }

                        echo '</td>';

                        echo '<td><select name="type" class="form-control">';
            
                            if($config->type == "post") {
                                echo '<option value="post" selected="selected">post</option>';
                                echo '<option value="env">env</option>';
                            }

                            if($config->type == "env") {
                                echo '<option value="post" >post</option>';
                                echo '<option value="env" selected="selected">env</option>';
                            }
  
                        
                        echo '</select></td>';

                        echo '<td > <input class="form-control" type="text" name="description" value="' . $config->description . '" ></td>';
                        echo '<td > <textarea class="form-control" rows="5" name="value" maxlength="999" >' . $config->value . '</textarea></td>';
                            
                        echo '<td>' . '<button  type="submit" class="btn btn-info btn-sm">save</button>' . '</td>';
                        echo '<td>' . '<a class="btn btn-danger btn-sm" href="./action/action_config.php?action=delete&configid='.$config->ID.'" >x</a>' . '</td>';
                        echo '</tr></form>';
                    }

                    ?>

                    </tbody>
                </table>



            </div>

        </div>

    </div>

</div>



<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>

</html>
