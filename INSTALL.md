# Installing wicopa

```bash
git clone https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa.git

apt-get install -y apache2 mariadb-server php7.0 phpmyadmin libapache2-mod-php7.0 pv

a2enmod php7.0

cd /var/www/html/wicopa

cp inc/conf/Conf.php.sample inc/conf/Conf.php

chown -R www-data /var/www/html/wicopa
chmod -R o-r /var/www/html/wicopa

service apache2 restart
```

# Creating username and database

```bash
echo "CREATE DATABASE wicopa;" | mysql -uroot -p

echo "CREATE USER 'wicopauser'@'localhost' IDENTIFIED BY 's3cret;'"  | mysql -uroot -p

echo 'GRANT ALL PRIVILEGES ON wicopa.* TO "wicopauser"@"localhost";' | mysql -uroot -p

wget http://web.mbb.univ-montp2.fr/download/wicopa.sql.gz

pv wicopa.sql.gz | gunzip | mysql -uwicopauser -p wicopa

vim inc/conf/Conf.php
```

# Using Docker

```bash
git clone https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa.git
# you need to have wicopa.sql here
cd wicopa
wget http://web.mbb.univ-montp2.fr/download/wicopa.sql.gz && gunzip -d wicopa.sql.gz

sudo docker-compose up --build
# you can access to it at http://127.0.0.1:8000
# you can login as "admin" with password w1c0Pa5s
```

Your database will be located in `.docker/db/wicopa`.
Note: the `:z` option on your volumes in the docker-compose file is needed if you run it on RedHat-like distros, due to some selinux problems [See here for more informations](https://www.projectatomic.io/blog/2015/06/using-volumes-with-docker-can-cause-problems-with-selinux/).
