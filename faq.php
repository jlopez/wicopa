<?php


session_start ();

require_once "./inc/php/buildHeader.php";

?>

<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<div class="container">

    <br/>

    <div class="row justify-content-center">

        <h1> FAQ </h1>

<div class="container py-3">
    <div class="row">
        <div class="col-10 mx-auto">
            <div class="accordion" id="faqExample">
                <div class="card">
                    <div class="card-header p-2" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Q: How does this work ?
                            </button>
                          </h5>
                    </div>

                    <div id="collapseOne" class="show" aria-labelledby="headingOne" data-parent="#faqExample">
                        <div class="card-body">
                            It browses public repositories and then, checks the needed packages. Wicopa is coded with PHP/JS and a SQL database.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header p-2" id="headingTwo">
                        <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                          Q: What does Wicopa mean and what is the main purpose of it ?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseTwo" class="show" aria-labelledby="headingTwo" data-parent="#faqExample">
                        <div class="card-body">
                          <b>Wicopa</b> means <i>Web Interface for Container Packaging</i>. It has been designed in order to help basic users to package applications using singularity or docker. We know that docker containers should be as small as possible, however some people could have different use cases, particularly when you do not need to use it as a service.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header p-2" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                              Q: Is it OpenSource ? Can I clone and modify it ?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="show" aria-labelledby="headingThree" data-parent="#faqExample">
                        <div class="card-body">
                            Yes it is OpenSource ! Obviously, you can modify it. It is licensed under <a href=https://www.apache.org/licenses/>Apache v2 License</a> terms.
                            The code is avalaible <a href="https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa">here</a>. You can clone it with <code>git clone https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa</code>. Every contributions are welcome.
                        </div>
                    </div>
                </div>
            
	      </div>

        </div>
    </div>
    <!--/row-->
</div>
<!--container-->


    </div>


</div>


</body>
</html>
