# wicopa

Wicopa means **Web Interface for Container PAckaging**

This Web UI application can generate Dockerfiles or definition files for Singularity.

# Install

Check [INSTALL](INSTALL.md)

You can test it easily with Docker, using `docker-compose` ([See here](INSTALL.md#using-docker)).


# Documentation

You can generate documentation with `doxygen .doxygen`

# Roadmap

  - Tags to search recipes ([see issue #11](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/11)),
  - Loading and sharing recipes ([see issue #10](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/10)),
  - Adding a custom local configuration to all recipes. Indeed, HPC administrator could occasionnally allow Singularity containers on their cluster. Thus, they usually need to edit the recipe to add custom contents, in order to rebuild the image ([see issue #7](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/7)),
  - Convert the functions to retrieve specific language packages in pure PHP to be able to upgrade it directly from the admin panel ([see issue #2](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/2)),
  - adding a connection to a personal Gitlab/Github profile ([see issue #13](https://gitlab.mbb.univ-montp2.fr/jlopez/wicopa/issues/13)).
