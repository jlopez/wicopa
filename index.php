<?php

session_start ();

require_once "./dao/DBquery.php";
require_once "./inc/php/printPanel.php";

$db = new DBquery();

$section = $db->getAllActiveSectionParent();

require_once "./inc/php/buildHeader.php";

?>

    <div class="container-fluid">

        <br/>

        <div class="row">
            <div class="col-4">

                <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Parameters :
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="imageNameInput" class="font-weight-bold">Image Name <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                            <input type="text" class="form-control" id="imageNameInput">
                        </div>



                        <div class="form-group">
                            <label for="selectedOS" class="font-weight-bold">From Distribution <span style="font-size:20px;" class="status text-danger">*</span>:</label>
                            <select class="custom-select" id="selectedOS" onchange="hide_os_rows()">

                                <?php
                                    foreach ($db->getActiveDistributionsFullName() as $name){
                                        if(strpos($name, "ubuntu") !== false) {
                                            echo ' <option value="'.$name.'" selected="selected">'.$name.'</option>';
                                        }
                                        else {
                                            echo ' <option value="'.$name.'">'.$name.'</option>';
                                        }
                                    }
                                ?>

                            </select>
                        </div>
                        

                        <div class="form-group">

                        <p class="font-weight-bold">OR</p>

                        </div>

                        <div class="form-group">
                            <label for="baseNameInput" class="font-weight-bold">From Image :</label>
                            <input type="text" class="form-control" id="baseNameInput">
                            <small id="help0" class="form-text text-muted">You can find images name on <a href="https://hub.docker.com/search?q=&type=image" >docker hub </a>.</small>
                            <small id="help1" class="form-text text-muted">If the above "From Image" field is empty, then <a href="https://docs.docker.com/engine/reference/builder/#from">"From Distribution"</a> is chosen from distribution bellow. </small>
                            <small id="help2" class="form-text text-muted">You must make sure that the OS of your base container is compatible with the chosen distribution. </small>
                        </div>

                        <div class="form-group">
                            <label for="selectedOS" class="font-weight-bold" hidden >Package manager :</label>
                            <select class="custom-select" id="selectedInstaller" hidden>

                            <?php
                                    foreach ($db->getManagers() as $manager){
                                        echo '<option value="'.$manager->value.'">'.$manager->value.'</option>';
                                    }
                            ?>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="containerType" class="font-weight-bold">Container type :</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="singularityRadio" name="customRadio" class="custom-control-input" checked>
                                <label class="custom-control-label" for="singularityRadio">Singularity</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="dockerRadio" name="customRadio" class="custom-control-input">
                                <label class="custom-control-label" for="dockerRadio">Docker</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" id="generateButton" class="btn btn-primary">Generate recipe</button>
                            <button type="button" id="downloadButton" class="btn btn-primary" onclick="downloadFile()">Download recipe</button>
                            <button type="button" id="generateButton" class="btn btn-primary" onclick="buildCommand()">Build command</button>
                            <!--<button type="button" id="generateButton" class="btn btn-primary">Push recipe</button>-->
                        </div>

                        <div class="form-group">

                        <?php 

                            if(isset($_SESSION['username']) && !empty($_SESSION['username']))
                            {
                                echo '<button type="button" class="btn btn-success" onclick="showFormContainer()">Publish recipe</button>';
                            }

                        ?>
                        </div>
                    </div>
                </div>

                </div>
                <br/>
                <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Selected packages  :
                    </div>
                    <div class="card-body">
                        <table id="Table_SelectedPackages" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th></th>
                                <th>From</th>
                                <th>Name</th>
                     
                            </tr>
                            </thead>
                            <tbody id="TableSelectedPackages">

                            </tbody>
                        </table>
                    </div>
                </div>
                </div>

                <br/>
                <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Local Environment configs  :
                    </div>
                    <div class="card-body">
                        <table id="Table_EnvConfig" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Active</th>
                                <th>Description</th>
                                <th>Value</th>
                     
                            </tr>
                            </thead>
                            <tbody id="TableEnvConfig">
                            <?php
                                foreach ($db->getActiveConfigsWithType("env") as $config) {
                                    echo '<tr>';
                                    echo '<td>';
                                    
                                    if($config->active == 1) {
                                        echo '<input type="checkbox" name="active" value="'.$config->ID.'" checked>';
                                    } else {
                                        echo '<input type="checkbox" name="active" value="'.$config->ID.'">';
                                    }
                                    echo '</td>';
                                    echo '<td>'.$config->description.'</td>';
                                    echo '<td>'.$config->value.'</td>';
                                    echo'</tr>';
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>

                <br/>
                <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Local Post configs  :
                    </div>
                    <div class="card-body">
                        <table id="Table_PostConfig" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Active</th>
                                <th>Description</th>
                                <th>Value</th>
                     
                            </tr>
                            </thead>
                            <tbody id="TablePostConfig">

                            <?php
                                foreach ($db->getActiveConfigsWithType("post") as $config) {
                                    echo '<tr>';
                                    echo '<td>';
                                    
                                    if($config->active == 1) {
                                        echo '<input type="checkbox" name="active" value="'.$config->ID.'" checked>';
                                    } else {
                                        echo '<input type="checkbox" name="active" value="'.$config->ID.'">';
                                    }
                                    echo '</td>';
                                    echo '<td>'.$config->description.'</td>';
                                    echo '<td><textarea class="form-control" rows="5" readonly>'.$config->value.'</textarea></td>';
                                    echo'</tr>';
                                }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                </div>

                <br/>
                <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Timezone  :
                    </div>
                    <div class="card-body">
                    <input type="text" class="form-control" id="timezonePreview" value='TZ=Europe/Paris; ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone'>
                    </div>
                </div>
                </div>

                <br/>

                <br/>
                <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Locale  :
                    </div>
                    <div class="card-body">
                    <input type="text" class="form-control" id="localePreview" value="sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen &&   echo 'LANG=&quot;en_US.UTF-8&quot;' >> /etc/default/locale">
                    </div>
                </div>
                </div>

                <br/>

                <div class="col-12">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        The default command line executed when you run the container :
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="ExecLineInput" class="font-weight-bold">Exec Line (<a href="https://singularity.lbl.gov/docs-docker#how-does-the-runscript-work">%Runscript</a> / <a href="https://docs.docker.com/engine/reference/builder/#cmd">CMD</a>) :</label>
                            <input type="text" class="form-control" id="ExecLineInput" value='exec /bin/bash "$@"'>
                        </div>

                    </div>
                </div>
                </div>

            </div>

            <div class="col-8">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-light">
                        Tools from packages manager :
                    </div>
                    <div class="card-body">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">

                                <?php
                                $index = 0;
                                foreach ($section as $s) {
                                    if($index == 0) {
                                        echo '<a class="nav-item nav-link active" id="nav-'.$s->name.'-tab" data-toggle="tab" href="#nav-'.$s->name.'" role="tab" aria-controls="nav-'.$s->name.'" aria-selected="false">'.$s->visual.' packages manager</a>';
                                    } else {
                                        echo '<a class="nav-item nav-link" id="nav-'.$s->name.'-tab" data-toggle="tab" href="#nav-'.$s->name.'" role="tab" aria-controls="nav-'.$s->name.'" aria-selected="false">'.$s->visual.'</a>';
                                    }
                                    $index++;
                                }
                                ?>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <?php
                            $index = 0;
                            foreach ($section as $s) {
                                if($index == 0) {
                                    echo '<div class="tab-pane fade show active" id="nav-'.$s->name.'" role="tabpanel" aria-labelledby="nav-'.$s->name.'-tab">';
                                }else {
                                    echo '<div class="tab-pane fade" id="nav-'.$s->name.'" role="tabpanel" aria-labelledby="nav-'.$s->name.'-tab">';
                                }
                                printSection($s->name, $db);
                                echo '</div>';
                                $index++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <br/>


            <div class="col-12">
                <div class="card border border-dark" id = "containerPreviewHead">
                    <div class="card-header bg-dark text-light">
                        Preview File :
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <textarea id="containerPreview"  class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Start Modal Publish -->
        <div class="modal" id="modalSaveRecipe">
            <div class="modal-dialog">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                <h4 class="modal-title">Publish container file</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="./action/action_container.php" method="post">
                    <!-- Modal body -->
                    <div class="modal-body">
                        
                        <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="create">
                        <textarea type="text" rows="5" class="form-control" id="valueContainer" style="display: none;" name="value" value=""></textarea>
                            <div class="form-group">
                                <label for="nameContainer">Name :</label>
                                <input type="text" class="form-control" id="nameContainer" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="typeContainer">Type :</label>
                                <input type="text" class="form-control" id="typeContainer" name="type" value="" readonly>

                            </div>
                            <div class="form-group">
                                <label for="visibilityContainer">Visibility :</label>
                                <select class="form-control" id="visibilityContainer" name="visibility">
                                    <option value="1">Public</option>
                                    <option value="0">Private</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="descriptionContainer">Description :</label>
                                <textarea class="form-control" rows="5" id="descriptionContainer" maxlength="500" name ="description" required></textarea>
                            </div> 
                            <div class="form-group">
                                <label for="labelsContainer">Labels :</label>
                                <select class="form-control" id="labelsContainer" name="labels[]" multiple>
                                <?php
                                    foreach ($db->getLabelsWithGrade($db->getGradeWithLogin($_SESSION['username'])) as $label){
                                        echo '<option value="'.$label->ID.'">'.$label->name.'</option>';
                                    }
                                ?>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="tagsContainer">Tags :</label>
                                <textarea class="form-control" rows="5" id="tagsContainer" maxlength="500" name ="tags"></textarea>
                            </div> 
                    
                    </div>


                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </form>
                
            </div>
            </div>
        </div>  <!-- End Modal Publish -->

        <!-- Start Modal Publish -->
        <div class="modal" id="modalBuildCommand">
            <div class="modal-dialog">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                <h4 class="modal-title">Build Command</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        
                    <div class="form-group">
                        <input type="text" class="form-control" id="valueBuildCommand" readonly><br/>
                    </div>

                    <div class="form-group">
                        <span id="copied" class="copiedtext text-center" style="display:none; font-weight: bold; margin: 0 auto;">Copied</span>
                    </div>
                    </div>


                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button id="copy" type="button" class="btn btn-success" onclick="copieCommand()">Copy</button>

                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>


                
            </div>
            </div>
        </div>  <!-- End Modal Publish -->
       
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>

    <script src="./inc/js/wicopa.js"></script>
    <script>

        $(function()
        {

            SELECTED_PACKAGES["Distribution"] = [];

            <?php
                foreach ($db->getActiveSection() as $s) {
                    echo 'SELECTED_PACKAGES["'.$s->name.'"] = [];';
                    echo 'SELECTED_PACKAGES_COLOR["'.$s->name.'"] = "' . $s->color . '";' ;
                    echo '
                    ';
                }
            ?>

            hide_os_rows();
        });


        function hide_os_rows() {
            var e = document.getElementById("selectedOS");
            var selectedOSRelease = e.options[e.selectedIndex].value;

            var res = "tr.table_container_" + selectedOSRelease.replace(":", "_").replace(".", "_");

            for (var i = 0; i < e.length; i++) {

                res = "tr.table_container_" + e[i].value.replace(":", "_").replace(".", "_");
                if (e[i].value !== selectedOSRelease) {
                    $(res).hide();
                } else {
                    $(res).show();
                }

            }

            var options = document.getElementById('selectedInstaller').getElementsByTagName('option');

            <?php

                foreach($db->getActiveDistributions() as $value){

                    echo "if(selectedOSRelease.includes(\"$value->name\")) {\n";

                    echo "for(var i = 0; i < options.length; i++)  {\n";
                            echo "if(options[i].value == \"$value->manager\") {\n";
                                echo "document.getElementById('selectedInstaller').getElementsByTagName('option')[i].selected = 'selected'\n";
                            echo "}\n";
                    echo "}\n";

                    echo "}\n";
                }

            ?>

            document.getElementById("selectedInstaller").disabled = true;
            
            gestionPackageSelected();
        }
        

    </script>
  </body>
</html>
