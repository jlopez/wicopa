<?php

/**
 * @file printPanel.php
 */

/**
 * Print Section with name of this Section and database
 * 
 * @param string $name The name of the Section to print
 * @param DBquery $db The database object
 */
function printSection($name, $db) {

  if($name == "Python") {
    echo '
      <div class="form-group">
        <label for="selectedPython" class="font-weight-bold">Python version :</label>
          <select class="custom-select" id="selectedPython">
            <option value="0" selected="selected">None</option>
            <option value="3.7" >3.7</option>
            <option value="3.6" >3.6</option>
            <option value="3.5" >3.5</option>
            <option value="2.7" >2.7</option>
          </select>
      </div>
    ';

    printPanel($name);
  } else if($name == "CPAN") {

    echo '
      <div class="form-group">
        <label for="localePerl" class="font-weight-bold">Perl locale :</label>
        <input type="text" class="form-control" id="localePerl" value="locale-gen en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 LANGUAGE=en_US:en">
      </div>
    ';
    printPanel($name);
  } else if($name == "R") {
      $subsection = $db->getNameSectionWhithParent("R");     
                                      
      echo '
          <div class="form-group">
              <label for="selectedR" class="font-weight-bold">R version :</label>
              <select class="custom-select" id="selectedR">
                  <option value="0" selected="selected" >None</option>
                  <option value="4.0.1">4.0.1</option>
                  <option value="3.6.3">3.6.3</option>
                  <option value="3.5.0">3.5.0</option>
              </select>
          </div>

          <div class="form-group">
            <label for="localeR" class="font-weight-bold">R locale :</label>
            <input type="text" class="form-control" id="localeR" value="en_US.UTF-8">
          </div>
          
          <nav>
              <div class="nav nav-tabs" id="nav-tab-R" role="tablist">
      ';

      echo '<a class="nav-item nav-link active" id="nav-'.$subsection[0].'-tab-R" data-toggle="tab" href="#nav-'.$subsection[0].'-R" role="tab" aria-controls="nav-'.$subsection[0].'-R" aria-selected="false">'.$subsection[0].'</a>';
      for ($i = 1; $i < sizeof($subsection); $i++) {
          echo '<a class="nav-item nav-link" id="nav-'.$subsection[$i].'-tab-R" data-toggle="tab" href="#nav-'.$subsection[$i].'-R" role="tab" aria-controls="nav-'.$subsection[$i].'-R" aria-selected="false">'.$subsection[$i].'</a>';
      }

      echo '
                  </div>
              </nav>
          <div class="tab-content" id="nav-tabContent-R">
      ';


      $indexSub = 0;
      foreach ($subsection as $sub) {
          if ($indexSub == 0) {
              echo '<div class="tab-pane fade show active" id="nav-' . $sub . '-R" role="tabpanel" aria-labelledby="nav-' . $s . '-tab-R">';
          } else {
              echo '<div class="tab-pane fade" id="nav-' . $sub . '-R" role="tabpanel" aria-labelledby="nav-' . $s . '-tab-R">';
          }

          
          printPanel($sub);
        

          echo '</div>';
          $indexSub++;
      }

      echo '</div>';
  } else {
    printPanel($name);
  }
}

/**
 * Print panel 
 * 
 * @param string $name THe name of the section
 */
function printPanel($name) {
  echo '
    <label for="find'.$name.'Packages">Find packages containing substring :</label>
    <input type="email" class="form-control" id="find'.$name.'Packages">';


    if($name == "CRAN") {
      echo '<small class="form-text text-muted">exemple: genepop</small>';
    }
    else if($name == "Bioconductor") {
      echo '<small class="form-text text-muted">exemple: GenomicFeatures</small>';
    }
    else if($name == "CPAN") {
      echo '<small class="form-text text-muted">exemple: cpanminus</small>';
    }
    else if ($name == "Rubygems") {
      echo '<small class="form-text text-muted">exemple: math-api</small>';
    }
    else if ($name == "Python") {
      echo '<small class="form-text text-muted">exemple: math3d</small>';
    }
    else if ($name == "NPM") {
      echo '<small class="form-text text-muted">exemple: math</small>';
    }
    else {
      echo '<small class="form-text text-muted">exemple: python</small>';
    }


    echo '
    
    <!-- 
    <div class="form-check">
      <input type="checkbox" class="form-check-input" id="pattern'.$name.'Name" checked>
      <label class="form-check-label" for="pattern'.$name.'Name">Pattern on name</label>
    </div>
  
    <div class="form-check">
      <input type="checkbox" class="form-check-input" id="pattern'.$name.'Description" checked>
      <label class="form-check-label" for="pattern'.$name.'Description">Pattern on description</label>
    </div> -->

    <br/>
  
    <!-- 
    <div class="form-group">
      <label for="limit'.$name.'" class="font-check-label">Max found :</label>
      <select class="custom-select" id="limit'.$name.'">
        <option value="5">5</option>
        <option value="10" selected="selected">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="100">200</option>
        <option value="1000000000">all</option>              
      </select>
    </div> -->

    <div class="form-group">
      <button type="button" class="btn btn-primary" onclick="showPackage(\''.$name.'\')">Search</button>
    </div>

    <div id="containPackages'.$name.'">

    </div>
  ';
}