
<?php 
echo '
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" crossorigin="anonymous">

    <!-- local fallbacks -->
    <!--link rel="stylesheet" href="./inc/css/bootstrap.min.css">
    <link rel="stylesheet" href="./inc/css/jquery.dataTables.min.css"-->

    <link rel="stylesheet" href="./inc/css/style.css">
    <script src="./inc/js/jscolor.js"></script>
    <title>wicopa</title>

</head>
<body>


<nav class="navbar navbar-expand-md navbar-dark navbar-custom p-1">
    <div class="navbar-brand">WICOPA (v0.5.1)</div>
    <div class="collapse navbar-collapse justify-content-between" id="navbar">
        <div class="navbar-nav">
        ';


            echo ' <a class="nav-item nav-link" href="./index.php"> Home </a>';


            echo ' <a class="nav-item nav-link" href="./container.php"> Contributed Containers </a>';

            if(isset($_SESSION['username']) && !empty($_SESSION['username']))
            {

                if(isset($_SESSION['manage']) && !empty($_SESSION['manage']))
                {
                    echo ' <a class="nav-item nav-link" href="./manage.php"> Manage </a>';
                    echo ' <a class="nav-item nav-link" href="./configuration.php"> Configs </a>';
                }
            }

            echo ' <a class="nav-item nav-link" href="./faq.php"> FAQ </a>';



        echo '
        </div>
        <div class="navbar-nav">
            ';
            if(isset($_SESSION['username']) && !empty($_SESSION['username']))
            {
                echo '<a class="nav-item btn btn-primary " href="./action/action_logout.php"> Logout ('.$_SESSION['username'].') </a>';
            }
            else
            {
                echo '<a class="nav-item btn btn-primary" href="./login.php"> Login </a>';
            }
           
        echo '</div>

</nav>

';

