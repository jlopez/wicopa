var SELECTED_PACKAGES = [];
var SELECTED_PACKAGES_COLOR = [];
var PREVIEW = "";


/**
 * Get the value of element
 * @param {string} id 
 */
function getValue(id) {
    return document.getElementById(id).value;
}

/**
 * Get if element is checked
 * @param {string} key 
 */
function getChecked(id) {
    return document.getElementById(id).checked
}

/**
 * Event when generate button is clicked
 */
$( "#generateButton" ).click(function() {
    if(SELECTED_PACKAGES["Python"].length > 0  && getValue("selectedPython") == 0) {
        alert("Please choose Python version");
    }
    else {
        generate_container();
    }
});

/**
 * Generate build command
 */
function buildCommand() {

    var cmd = "sudo ";

    var isSingularity = getChecked("singularityRadio");
    var filename = getValue("imageNameInput");

    if (isSingularity) {
        cmd += "singularity build ";

        if (filename === "") {
            cmd += "myimg.sif Singularity";
        }
        else {
            cmd += filename + ".sif " + filename + ".def";
        }
    }
    else {

        cmd += "docker build ";

        if (filename === "") {
            cmd += ".";
        }
        else {
            cmd += "-f " + filename + " .";
        }
    }

    document.getElementById("valueBuildCommand").value = cmd;

    $("#modalBuildCommand").modal();
}

function copieCommand() {
    var toCopy  = document.getElementById( 'valueBuildCommand' ),
    btnCopy = document.getElementById( 'copy' ),
    copied = document.getElementById( 'copied' );


    toCopy.select();
    
    if ( document.execCommand( 'copy' ) ) {
        //btnCopy.classList.add( 'copied' );
        copied.style.display = "block";
        console.log(copied.style.display);
        var temp = setInterval( function(){
            copied.style.display = "none";
            clearInterval(temp);
        }, 600 );
        
    } else {
        console.info( 'document.execCommand went wrong…' )
    }
        

}

/**
 * Function for donwload Singularity or Dockerfile file after user generate the file.
 */
function downloadFile() {

    if(PREVIEW !== "") {
        var filename = getValue("imageNameInput");
        var isSingularity = getChecked("singularityRadio");

        if (filename === "") {
            if (isSingularity) {
                filename = "Singularity";
            } else {
                filename = "Dockerfile";
            }
        }
        else {
            if (isSingularity) {
                filename += ".def";
            }
        }

        var text = getValue("containerPreview");
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    } else {
        alert("Generate once before download file");
    }
}


function downloadFile2(name, value) {
    var filename = name;
    var text = value;
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}


/**
 * Get the start keyword 
 * 
 * @param {bool} isSingularity True for Singularity file, otherwise false for Docker
 */
function getStartType(isSingularity) {
    if(isSingularity) {
        return "\t";
    }
    else {
        return "RUN ";
    }
}

/**
 * Generate container file
 * 
 * @return {string}
 */
function generate_container() {

    PREVIEW = "";
    
    var isSingularity = getChecked("singularityRadio");
    var startType = getStartType(isSingularity);
    var base = getValue("baseNameInput");

    if(base == "") {
        base = getValue("selectedOS");
    }

    var filename = getValue("imageNameInput");
    var manager = getValue("selectedInstaller");
    var rversion = getValue("selectedR");
    var pythonversion = getValue("selectedPython");
    var execline = getValue("ExecLineInput");

    var nrow = 2;

    //HEADER
    if(isSingularity) {
        PREVIEW += "Bootstrap: docker\n";
        PREVIEW += "From: "+base+"\n";
        PREVIEW += "IncludeCmd: yes\n";
        PREVIEW += "\n";
        nrow += 4;
    } else {
        PREVIEW += "FROM "+base+"\n";
        PREVIEW += "\n";
        nrow += 2;
    }

    //ENV
    if(isSingularity) {
        PREVIEW +="%environment\n";
        nrow += 1;
    }

    if(SELECTED_PACKAGES["Conda"].length > 0) {
        if(isSingularity) {
            PREVIEW += "\texport PATH=/opt/conda/bin:$PATH\n";
            PREVIEW += "\n";
            nrow += 2;
        } else {
            PREVIEW += "RUN export PATH=/opt/conda/bin:$PATH\n";
            PREVIEW += "\n";
            nrow += 2;
        }
    }

    //ENV ADMIN
    var table = document.getElementById("Table_EnvConfig");

    for(var i = 1; i < table.rows.length; i++) {
        var active = table.rows[i].cells[0].children[0];

        if(active.checked) {
            var value = table.rows[i].cells[2].innerHTML;

            var lines = value.split('\n');

            for(var j = 0; j < lines.length; j++) {
                var line = lines[j];

                if(line != "") {
                    if(isSingularity) {
                        PREVIEW += "\t"+ line + "\n";
                        nrow += 1;
                    } else {
                        PREVIEW += "RUN " + line + "\n";
                        nrow += 1;
                    }
                }
            }

            PREVIEW += "\n";
            nrow += 1;
        }
    
    }
    

    var date = new Date();

    var options = {
        year: "numeric", month: "short",
        day: "numeric"
    };

    var current_date = date.toLocaleTimeString("en-us", options);

    //LABEL
    if(isSingularity) {
        PREVIEW += "%labels\n";
        PREVIEW += "\tAuthor wicopa\n";
        PREVIEW += "\tVersion v0.5.1\n";
        PREVIEW += "\tbuild_date " + current_date + "\n";
        if(filename) {
          PREVIEW += "\tImage_name " + filename + "\n";
          PREVIEW += "\n";
          nrow += 6;
        } else {
          PREVIEW += "\n";
          nrow += 5;
        }
    } else {
        PREVIEW += "LABEL Author wicopa\n";
        PREVIEW += "LABEL Version v0.4.1\n";
        PREVIEW += "LABEL build_date " + current_date + "\n";
        if(filename) {
          PREVIEW += "LABEL Image_name " + filename + "\n";
          PREVIEW += "\n";
          nrow += 5;
        } else {
          PREVIEW += "\n";
          nrow += 4;
        }
    }

    if(isSingularity) {
        PREVIEW += "%post\n";
    }

    PREVIEW += startType + getValue("timezonePreview") + "\n\n";
    PREVIEW += startType + getValue("localePreview") + "\n\n";
    PREVIEW += startType + manager + " update\n\n";
    nrow += 6;

    for(var i = 0; i < SELECTED_PACKAGES["Distribution"].length; i++) {
        PREVIEW += startType+manager+" install -y " + SELECTED_PACKAGES["Distribution"][i][0] + "\n";
        nrow += 1;
    }

    if(rversion != "0") {

        PREVIEW += "\n############### Install R ##############\n";

        PREVIEW += startType+manager+" install -y " + "make gcc gfortran g++ libreadline-dev" + "\n";
        PREVIEW += startType+manager+" install -y zlib1g zlib1g-dev" + "\n";
        PREVIEW += startType+manager+" install -y bzip2 libbz2-dev" + "\n";
        PREVIEW += startType+manager+" install -y liblzma5 liblzma-dev" + "\n";
        PREVIEW += startType+manager+" install -y libpcre3 libpcre3-dev" + "\n";
        PREVIEW += startType+manager+" install -y libcurl4 curl libcurl4-openssl-dev" + "\n";
        
        if(isSingularity) {
            PREVIEW += '\tcd $HOME';
            PREVIEW += '\n\tcurl -L https://cran.rstudio.com/src/base/R-3/R-'+ rversion+ '.tar.gz';
            PREVIEW += '\n\ttar xvf R-'+ rversion+ '.tar.gz';
            PREVIEW += '\n\tcd R-'+ rversion + "\n";
            PREVIEW += "\t./configure --enable-R-static-lib --with-blas --with-lapack --enable-R-shlib=yes "+ "\n";
            PREVIEW += "\tmake"+ "\n";
            PREVIEW += "\tmake install"+ "\n";
        } else {
            PREVIEW += 'RUN cd $HOME \\';
            PREVIEW += '\n\t&& curl -L https://cran.rstudio.com/src/base/R-3/R-'+ rversion+ '.tar.gz \\';
            PREVIEW += '\n\t&& tar xvf R-'+ rversion+ '.tar.gz \\';
            PREVIEW += '\n\t&& cd R-'+ rversion+ ' \\' + "\n";
            PREVIEW += "\t&& ./configure --enable-R-static-lib --with-blas --with-lapack --enable-R-shlib=yes \\"+ "\n";
            PREVIEW += "\t&& make \\"+ "\n";
            PREVIEW += "\t&& make install "+ "\n";
        }
        nrow += 8;

        var localeR = getValue("localeR");

        if(localeR != "") {
            localeR = "echo 'Sys.setlocale(\"LC_ALL\", \"" + localeR + "\")' | R --slave";
            PREVIEW += startType + localeR + "\n\n";
            nrow += 2;
        }

    }

    for(var i = 0; i < SELECTED_PACKAGES["CRAN"].length; i++) {
        PREVIEW += "\n" + startType +'R --slave -e "install.packages(\'' +SELECTED_PACKAGES["CRAN"][i][0] +  '\', repos=\'https://cloud.r-project.org\')"';
        nrow += 1;
    }

    if(SELECTED_PACKAGES["Bioconductor"].length > 0) {

        PREVIEW += "\n\n############### Install Bioconductor ##############\n";

        PREVIEW += "\n" + startType +'R --slave -e "install.packages(\'BiocManager\')"';

        for(var i = 0; i < SELECTED_PACKAGES["Bioconductor"].length; i++) {
            PREVIEW += "\n" + startType +'R --slave -e "BiocManager::install(\''+SELECTED_PACKAGES["Bioconductor"][i][0]+'\')"';
            nrow += 1;
        }

        nrow += 2;
    }

    if(SELECTED_PACKAGES["Conda"].length > 0) {

        PREVIEW += "\n\n############### Install Conda ##############\n";

        //POST
        if(isSingularity) {
            
            PREVIEW += "\t"+manager+" update\n";

            PREVIEW += "\techo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh"+ "\n";
            PREVIEW += "\t"+manager+" -y install curl bzip2 \\" + "\n";
            PREVIEW += "\t&& curl -sSL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /tmp/miniconda.sh \\" + "\n";
            PREVIEW += "\t&& bash /tmp/miniconda.sh -bfp /usr/local/ \\" + "\n";
            //PREVIEW += "\t&& conda install -y python=3 \\" + "\n";
            PREVIEW += "\t&& conda config --add channels bioconda \\" + "\n";
            PREVIEW += "\t&& conda config --add channels anaconda \\" + "\n";
            PREVIEW += "\t&& conda config --add channels conda-forge \\" + "\n";
            PREVIEW += "\t&& conda update conda \\" + "\n";
            PREVIEW += "\t&& conda clean --all --yes " + "\n";

        } else {
            PREVIEW += "RUN "+manager+" update\n";

                PREVIEW += "RUN echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh" + "\n";

                PREVIEW += "RUN "+manager+" -y update \\" + "\n";
                PREVIEW += "&& "+manager+" -y install curl bzip2 \\" + "\n";
                PREVIEW += "&& curl -sSL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /tmp/miniconda.sh \\" + "\n";
                PREVIEW += "&& bash /tmp/miniconda.sh -bfp /usr/local/ \\" + "\n";
                //PREVIEW += "&& conda install -y python=3 \\" + "\n";
                PREVIEW += "&& conda config --add channels bioconda \\" + "\n";
                PREVIEW += "&& conda config --add channels anaconda \\" + "\n";
                PREVIEW += "&& conda config --add channels conda-forge \\" + "\n";
                PREVIEW += "&& conda update conda \\" + "\n";
                PREVIEW += "&& conda clean --all --yes " + "\n";

        }

        nrow += 11;

        for(var i = 0; i < SELECTED_PACKAGES["Conda"].length; i++) {

            value = SELECTED_PACKAGES["Conda"][i][0].split('/');

            if(value.length == 2) {

                author =  value[0];
                name = value[1];
                
                PREVIEW += "\n" + startType + "conda install -c " + author + " " + name + "=" + SELECTED_PACKAGES["Conda"][i][1];
                nrow += 1;
            }
        }
    }



    if(SELECTED_PACKAGES["Rubygems"].length > 0) {

        PREVIEW += "\n\n############### Install Rubygems ##############\n";

        PREVIEW += startType+manager+" install -y " + "ruby rubygems" + "\n";
        nrow += 1;

        for(var i = 0; i < SELECTED_PACKAGES["Rubygems"].length; i++) {
            PREVIEW += "\n" + startType + "gem install "+SELECTED_PACKAGES["Rubygems"][i][0] + " -v " + SELECTED_PACKAGES["Rubygems"][i][1];
            nrow += 1;
        }
    }

    if(SELECTED_PACKAGES["Python"].length > 0) {

        PREVIEW += "\n\n############### Install Python ##############\n";

        PREVIEW += startType+manager+" install -y " + "python" + pythonversion + "\n";
        PREVIEW += startType+manager+" install -y " + "python3-pip" + "\n";
        PREVIEW += startType+"pip install --upgrade pip" + "\n";
        nrow += 4;

        for(var i = 0; i < SELECTED_PACKAGES["Python"].length; i++) {
            PREVIEW += "\n" + startType + "pip install "+SELECTED_PACKAGES["Python"][i][0] + "==" + SELECTED_PACKAGES["Python"][i][1];
            nrow += 1;
        }
    }

    if(SELECTED_PACKAGES["NPM"].length > 0) {

        PREVIEW += "\n\n############### Install NPM ##############\n";

        PREVIEW += "\n" + startType + "curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -";
        PREVIEW += "\n" + startType + manager + " install -y nodejs\n";
        nrow += 3;

        for(var i = 0; i < SELECTED_PACKAGES["NPM"].length; i++) {
            var version = "@"+SELECTED_PACKAGES["NPM"][i][1];
            PREVIEW += "\n" + startType + "npm install -g "+SELECTED_PACKAGES["NPM"][i][0]+version;
            nrow += 1;
        }
    }

    if(SELECTED_PACKAGES["CPAN"].length > 0) {

        PREVIEW += "\n\n############### Install CPAN ##############\n";

        PREVIEW += "\n" + startType + "curl -L http://xrl.us/installperlnix | bash";
        PREVIEW += "\n" + startType + "curl -L https://cpanmin.us | perl - App::cpanminus\n";
        nrow += 2;
        var localePerl = getValue("localePerl");

        if(localePerl != "") {
            PREVIEW += startType + localePerl + "\n\n";
            nrow += 2;
        }

        for(var i = 0; i < SELECTED_PACKAGES["CPAN"].length; i++) {
            //var version = "@"+SELECTED_PACKAGES["CPAN"][i][1];
            var version = "";
            PREVIEW += "\n" + startType + "cpanm install "+SELECTED_PACKAGES["CPAN"][i][0]+version;
            nrow += 1;
        }
    }

    PREVIEW += "\n\n";
    nrow += 2;

    //POST ADMIN
    var table = document.getElementById("Table_PostConfig");

    if(table.rows.length > 0) {
        PREVIEW += "\n\n############### Local Post configs ##############\n";
    }

    for(var i = 1; i < table.rows.length; i++) {

        var rows = table.rows[i];
        var active = rows.cells[0].children[0];

        if(active.checked) {
            var value = rows.cells[2].children[0].value;
            var lines = value.split('\n');

            for(var j = 0; j < lines.length; j++) {
                var line = lines[j];
                if(line != "") {
                    PREVIEW += startType + line + "\n";
                    nrow += 1;
                }
            }

            PREVIEW += "\n";
            nrow += 1;
        }
    }
    
    PREVIEW += "\n\n\n";
    nrow += 3;

    //END

    if(execline != "") {
        if(isSingularity) {
            PREVIEW += "%runscript\n";
            PREVIEW += "\t"+execline.trim()+"\n";
            PREVIEW += "\n";
            nrow += 6;
        } else {
            PREVIEW += "CMD "+execline.trim()+"\n";
            PREVIEW += "\n";
            nrow += 2;
        }
    }
    else {
        if(isSingularity) {
            PREVIEW += "%apprun run\n";
            PREVIEW += "\texec /bin/bash \"$@\"\n";
            PREVIEW += "\n";
            PREVIEW += "%runscript\n";
            PREVIEW += "\texec /bin/bash \"$@\"\n";
            PREVIEW += "\n";
            nrow += 6;
        } else {
            PREVIEW += "CMD exec /bin/bash \"$@\"\n";
            PREVIEW += "\n";
            nrow += 2;
        }
    }

    $("#containerPreview").attr('rows', nrow);
    $("#containerPreview").text(PREVIEW);
    document.getElementById('containerPreview').focus();
}

function generatePackageSelected(Pthis) {

    var key = Pthis.dataset["psection"];
    var data = [Pthis.dataset["pname"], Pthis.dataset["pversion"], key, Pthis.id];

    if(key == "Distribution") {
        data = [Pthis.dataset["pname"], Pthis.dataset["pversion"], getValue("selectedOS"), Pthis.id];
    }

    var newSelected = [];

    if(Pthis.checked) {
        var add = true;
        for(var i = 0; i < SELECTED_PACKAGES[key].length; i++) {
            if(SELECTED_PACKAGES[key][i][0] === Pthis.dataset["pname"]) {
                add = false;
            }
            newSelected.push(SELECTED_PACKAGES[key][i]);
        }

        if(add) {
            newSelected.push(data);
        }

    } else {
        for(var i = 0; i < SELECTED_PACKAGES[key].length; i++) {
            if (SELECTED_PACKAGES[key][i][0] === Pthis.dataset["pname"]) {
                //nothing
            } else {
                newSelected.push(SELECTED_PACKAGES[key][i]);
            }
        }
    }

    SELECTED_PACKAGES[key] = newSelected;

    gestionPackageSelected();
}


function gestionPackageSelected() {
    var result = "";

    for (var key in SELECTED_PACKAGES) {
        var tab = SELECTED_PACKAGES[key];

        for(var i = 0; i < tab.length; i++) {
            result = result + "<tr><td><button type='button' data-pname='"+ tab[i][0] +"' data-pid = '"+ tab[i][3] +"' data-psection = '"+ tab[i][2] +"' onclick='removePackageButton(this)' class='btn btn-danger btn-sm'>x</button></td><td style=\"text-align: center;\"><span class=\"badge\" style=\"background-color: "+ SELECTED_PACKAGES_COLOR[key] + "; color: #fff;\" >"+ tab[i][2] +"</span></td><td>"+ tab[i][0] +"<span class=\"badge\" style=\"background-color: #2f3640; color: #FFFFFF; margin-left: 10px\" >"+ tab[i][1] +"</span></td></tr>";
        }
    }

    document.getElementById("TableSelectedPackages").innerHTML = result;
}


function removePackageButton(Pthis) {
    var newSelected = [];
    var key = Pthis.dataset["psection"];

    if(key == document.getElementById("selectedOS").value) {
        key = "Distribution";
    }

    for(var i = 0; i < SELECTED_PACKAGES[key].length; i++) {
        if (SELECTED_PACKAGES[key][i][0] === Pthis.dataset["pname"]) {
            //nothing
            if(document.getElementById(Pthis.dataset["pid"])) {
                document.getElementById(Pthis.dataset["pid"]).checked = false;
            }
        } else {
            newSelected.push(SELECTED_PACKAGES[key][i]);
        }
    }

    SELECTED_PACKAGES[key] = newSelected;

    gestionPackageSelected();
}


function showFormContainer() {
            
    if(PREVIEW !== "") {
        var isSingularity = document.getElementById("singularityRadio").checked;

        if(isSingularity) {
            document.getElementById("typeContainer").value = "Singularity";
        } else {
            document.getElementById("typeContainer").value = "Docker";
        }

        document.getElementById("valueContainer").value = PREVIEW; 

        $('#modalSaveRecipe').modal();

    } else {
        alert("Generate once before publish file");
    }

}

/**
 * Show all packages found with distrib or techno name
 * @param {string} distribName 
 */
function showPackage(distribName) {

    var distrib = distribName;

    if(distribName == "Distribution") {
        distrib = getValue("selectedOS");
    }

    var str = getValue("find"+distribName+"Packages");

    var limit = 30;

    if (str == "") {
        document.getElementById("containPackages"+distribName).innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("containPackages"+distribName).innerHTML = this.responseText;
            }
        };

        xmlhttp.open("GET","getPackages.php?distrib="+distrib+"&filter="+str+"&patternName="+true+"&patternDescription="+true+"&limit="+limit,true);
        console.log("getPackages.php?distrib="+distrib+"&filter="+str+"&patternName="+true+"&patternDescription="+true+"&limit="+limit);
        xmlhttp.send();
    }
}